<div class="kt-portlet kt-portlet--mobile" style="margin-top: 50px">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
            <h3 class="kt-portlet__head-title">
				 Manajemen User
			</h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
				@if($Created =='C')
                    <button onclick="add_action('user/create/{{$id_menu}}','_content_','Master Client');return false;" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-plus"></i>Tambah User</button>
				@endif
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <!--begin: Search Form -->

        <!--begin: Datatable -->

        <div class="table-responsive" style="margin-top: 15px">
			<table class="table table-bordered table-striped" id="order_table">
				<thead class="thead">
					<tr>
						<th>No.</th>
						<th>Kode User</th>
						<th>Nama</th>
						<th>Email</th>
						<th>Role</th>
						<th>Aktif</th>
						<th>Lock</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
            <!--end: Datatable -->
        </div>
    </div>
</div>

    <script>
        $(document).ready(function() {

            load_data();

            function load_data(from_date = '', to_date = '') {
                $('#order_table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: 'user/data',
                        data: {
                             idmenu: {{$id_menu}}
                        }
                    },
                    columns: [{data: 'DT_RowIndex', orderable: false, searchable: false
						},{
                            data: 'kode_user',
                            name: 'kode_user'
                        },{
                            data: 'name',
                            name: 'name'
                        }, {
                            data: 'email',
                            name: 'email'
                        }, {
                            data: 'nama_role',
                            name: 'nama_role'
                        }, {
                            data: 'is_active_ur',
                            name: 'is_active_ur'
                        }, {
                            data: 'is_lock',
                            name: 'is_lock'
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            }

        });
    </script>
