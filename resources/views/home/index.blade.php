<div class="kt-portlet kt-portlet--mobile" style="margin-top: 50px">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
            <h3 class="kt-portlet__head-title">
                Portal
			</h3>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="row padding-chart">
            <div class="col-md-3">
                <div class="card transparan">
                    <div class="position-relative mb-4">
                        <a href="#" class="small-box-footer">Link 1 <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>

            </div>
            <div class="col-md-3">
                <div class="card transparan">
                    <div class="position-relative mb-4">
                        <a href="#" class="small-box-footer">Link 2 <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card transparan">
                    <div class="position-relative mb-4">
                        <a href="#" class="small-box-footer">Link 3 <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card transparan">
                    <div class="position-relative mb-4">
                        <a href="#" class="small-box-footer">Link 4 <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
        </div>

        {{--
        <div class="table-responsive" style="margin-top: 15px">


        </div> --}}
    </div>

    <div class="kt-portlet__body">
        <div class="wrapper padding-header">
            <div class="row padding-chart">
                <div class="col-md-12">
                    <div class="card transparan">
                        <div class="position-relative mb-4">
                            <div id="id_chart_product" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="wrapper padding-header">
            <div class="row padding-chart">
                <div class="col-md-12">
                    <div class="card transparan">
                        <div class="position-relative mb-4">
                            <div id="id_chart_materi" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="wrapper padding-header">
            <div class="row padding-chart">
                <div class="col-md-12">
                    <div class="card transparan">
                        <div class="position-relative mb-4">
                            <div id="id_chart_toko" style="height: 300px"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<script src="{{asset('js/dashboard/product.js')}}"></script>
<script src="{{asset('js/dashboard/materi.js')}}"></script>
<script src="{{asset('js/dashboard/toko.js')}}"></script>


<script>

	$(document).ready(function() {
        chart_product();
        chart_materi();
        chart_toko();

	});

    function chart_product(){
        var baseUrl         = "{{url('home/chart_product')}}"
        $.ajax({
            url: baseUrl,
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            type: 'POST',
            dataType: 'JSON',
            data: {}
        }).done(function(result) {
            product('id_chart_product',result);
        });
    }

    function chart_materi(){
        var baseUrl         = "{{url('home/chart_materi')}}"
        $.ajax({
            url: baseUrl,
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            type: 'POST',
            dataType: 'JSON',
            data: {}
        }).done(function(result) {
            materi('id_chart_materi',result);
        });
    }

    function chart_toko(){
        var baseUrl         = "{{url('home/chart_toko')}}"
        $.ajax({
            url: baseUrl,
            headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
            type: 'POST',
            dataType: 'JSON',
            data: {}
        }).done(function(result) {
            toko('id_chart_toko',result);
        });
    }




</script>
