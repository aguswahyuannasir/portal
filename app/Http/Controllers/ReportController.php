<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB,Excel,Redirect,Validator;
Use DataTables;
use App\Http\Traits\ActMain;
use App\Http\Traits\Helper;
use App\Exports\ExportExcel;
use Response;


class ReportController extends Controller
{
    use ActMain;
    use Helper;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable

     */

    function index(Request $request)
    {

		$input = $request->all();
		$id_menu = $request->input('idmenu');
		$IdMsRole = session('IdMsRole');
		$KdUser = session('KdUser');

		$this->set_log($id_menu,$KdUser);

		$MsReport = DB::table('MsReport')->select('*')->where('IdMsMenu', '=', $id_menu)->first();

		$JudulHeader = $MsReport->NmReport;//dd($Header);
		$IdMsReport = $MsReport->IdMsReport;//dd($Header);


		$MsRole = DB::table('MsRoleMenu')->select('*')->where('IdMsMenu', '=', $id_menu)->where('IdMsRole', '=', $IdMsRole)->first();
		$Download = $MsRole->Download;

		$param = DB::select("select b.IdMsParameter, b.IdMsReport, b.Label, b.DataField, b.DataType,
							b.Operator, b.SelectedValue, b.DropdownQuery, b.IsActive, b.Code, b.Urai from MsReport a
							left join MsParameter b on a.IdMsReport  = b.IdMsReport
							where a.IdMsMenu = '".$id_menu."' order by b.OrderBy ASC" );//dd($param);

		$html = '';
		foreach ($param as $val) {
			$DataType = $val->DataType;
			$SelectedValue = $val->SelectedValue;
			if($DataType == 'dropdown'){
				if($SelectedValue != ''){
					$selected = $SelectedValue;
				}else{
					$selected = '';
				}

				if($val->DropdownQuery == 'get_kpa'){
				$html .='<div class="col-lg-6 kt-margin-b-10-tablet-and-mobile" style="margin-bottom:15px;">
					<label>'.$val->Label.':</label>
					    <select class="form-control select2" id="'.$val->DataField.'" name="'.$val->DataField.'" >
                                     <option value="">--semua--</option>
                        </select>
				</div>';
				}else{
				$DataField = $this->get_arr_combobox("".$val->DropdownQuery."", "$val->Code", "$val->Urai", TRUE);
				$val->DataField = $this->form_dropdown(''.$val->DataField.'', $DataField, $selected, 'id="'.$val->DataField.'"  class="select form-control select2" onchange="get_kpa(this.value,)"');

				$html .='<div class="col-lg-6 kt-margin-b-10-tablet-and-mobile" style="margin-bottom:15px;">
					<label>'.$val->Label.':</label>
					  '.$val->DataField.'
				</div>';
				}


			}elseif($DataType == 'fromto'){
				$html .='<div class="col-lg-6 kt-margin-b-10-tablet-and-mobile" style="margin-bottom:15px;">
					<label>'.$val->Label.':</label>
					<div class="input-daterange input-group" id="kt_datepicker">
						<input type="text" data-date-format="yyyy-mm-dd" class="form-control kt-input" name="'.$val->DataField.''.$val->Code.'" id="TglSetFrom" placeholder="From" data-col-index="5" required="required">
						<div class="input-group-append">
							<span class="input-group-text"><i class="la la-ellipsis-h"></i></span>
						</div>
						<input type="text"  data-date-format="yyyy-mm-dd" class="form-control kt-input" id="TglSetTo" name="'.$val->DataField.''.$val->Urai.'" placeholder="To" data-col-index="5" required="required">
					</div>
				</div>';
			}else{
			$html .= 	'<div class="col-lg-6 kt-margin-b-10-tablet-and-mobile" style="margin-bottom:15px;">
							<label>'.$val->Label.':</label>
							<input type="text" name="'.$val->DataField.'" id="'.$val->DataField.'" class="form-control kt-input" data-col-index="0">
						</div>';
			}
		}
		$html .= 	'<input type="hidden" name="menu_id" id="menu_id" value="'.$id_menu.'" class="form-control kt-input" data-col-index="0">';$html .= 	'<input type="hidden" name="KdUser" id="KdUser" value="'.$KdUser.'" class="form-control kt-input" data-col-index="0">';
		$html .= 	'<input type="hidden" name="tipe_file" id="tipe_file" value="csv" class="form-control kt-input" data-col-index="0">';

		if($Download == 'D'){
			$buttonaksi = '<button type="button" class="btn btn-success btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="la la-download"></i> Unduh
                        </button>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul class="">
									<button onclick="myFunctionexcel(\'FomrSearch\');" id="export_excel" class="btn btn-success" style="margin-left: -40px;">Export Excel</button>
									<button onclick="myFunctioncsv(\'FomrSearch\');" id="export_csv" class="btn btn-warning">Export CSV</button>
                            </ul>
                    </div>';
		}else{
			$buttonaksi = '';
		};

		$html .= ' <div class="col-lg-3 kt-margin-b-10-tablet-and-mobile" style="margin-top: 20px">
                   <button class="btn btn-primary btn-brand--icon" id="search">
                        <span>
							<i class="la la-search"></i>
							<span>Tampil</span>
                        </span>
                    </button>
                    &nbsp;&nbsp;
                   		'.$buttonaksi.'
                    ';

		$header = DB::select("SELECT IdMsHeaderReport, IdMsReport, TypeReport, Label, DataField, DataType, Format, IsActive
								FROM MsHeaderReport where IdMsReport = '".$IdMsReport."' and TypeReport = '1'" );//dd($header);
		$content = '[';
		$tr = '<tr>';
		foreach ($header as $val) {
			$content .= 	'{
                            data: "'.$val->DataField.'",
                            name: "'.$val->DataField.'"
							},';
			$tr .= '<th>'.$val->Label.'</th>';
		}
		$tr .= '</tr>';
		$content .= "]"; //dd($content);


		return view('report' ,['id_menu' => $id_menu,'content' => $content,'tr' => $tr,'html' => $html, 'JudulHeader' => $JudulHeader]);
    }

   function data(Request $request)
    {
      if(request()->ajax())
      {

		$arrfield  = array();
		$arrvalue  = array();
		$data = $request->formData;
		$KdUser = session('KdUser');
		$start = $request->start;
		foreach ($data as $val) {
			if($val['name'] != '_token' && $val['name'] != 'order_table_length' && $val['name'] != 'menu_id'&& $val['name'] != 'tipe_file'  ){
				$arrfield[$val['name']]  = $val['value']==NULL?'ALL':$val['value'];
			}
		}	//dd($arrfield);
		$json = json_encode($arrfield);

		$id_menu = $request->id_menu;
		$MsReport = DB::table('MsReport')->select('*')->where('IdMsMenu', '=', $id_menu)->first();

		$Query = $MsReport->Query;

		$explode = explode(';', $Query);
		$exp1 = $explode[0];
		$exp2 = $explode[1];
		$result = str_replace("@json","'$json'",$exp1);

		if(count($explode) > 1){
			if($start == 0){
				 DB::update($result);
			}
		}

		$select = explode('#', $exp2);
		$select1 = $select[0];
		$select2 = $select[1];
		$result2 = DB::table($select1)->where('KdUser', $KdUser);


			return Datatables::of($result2)
							->editColumn('TglSet', function ($result2) {
											if (empty($result2->TglSet)) {
												return '';
											}
											return date('d-m-Y', strtotime($result2->TglSet) );
										})
							->editColumn('AmtPremiPokokDibayar', function ($result2) {
											if (empty($result2->AmtPremiPokokDibayar)) {
												return '';
											}
											return number_format($result2->AmtPremiPokokDibayar,2,',','.');
										})
							->editColumn('AmtTopUpDibayar', function ($result2) {
											if (empty($result2->AmtTopUpDibayar)) {
												return '';
											}
											return number_format($result2->AmtTopUpDibayar,2,',','.');
										})
							->editColumn('AmtTUIRDibayar', function ($result2) {
											if (empty($result2->AmtTUIRDibayar)) {
												return '';
											}
											return number_format($result2->AmtTUIRDibayar,2,',','.');
										})
							->editColumn('AmtKomisiPremiPokok', function ($result2) {
											if (empty($result2->AmtKomisiPremiPokok)) {
												return '';
											}
											return number_format($result2->AmtKomisiPremiPokok,2,',','.');
										})
							->editColumn('AmtKomisiTopUp', function ($result2) {
											if (empty($result2->AmtKomisiTopUp)) {
												return '';
											}
											return number_format($result2->AmtKomisiTopUp,2,',','.');
										})
							->editColumn('AmtKomisiTUIR', function ($result2) {
											if (empty($result2->AmtKomisiTUIR)) {
												return '';
											}
											return number_format($result2->AmtKomisiTUIR,2,',','.');
										})
							->editColumn('TotalPremi', function ($result2) {
											if (empty($result2->TotalPremi)) {
												return '';
											}
											return number_format($result2->TotalPremi,2,',','.');
										})
							->editColumn('TotalKomisi', function ($result2) {
											if (empty($result2->TotalKomisi)) {
												return '';
											}
											return number_format($result2->TotalKomisi,2,',','.');
										})
							->editColumn('AmtPremiExtra', function ($result2) {
											if (empty($result2->AmtPremiExtra)) {
												return '';
											}
											return number_format($result2->AmtPremiExtra,2,',','.');
										})
							->toJson();

      }

    }


	public function export_excel(Request $request) {//dd('sssdfs');

		$request = $request->all();
		$tipe_file = $request['tipe_file'];
		$id_menu = $request['menu_id'];
		$NoPolis = $request['NoPolis'];

		$MsReport = DB::table('MsReport')->select('*')->where('IdMsMenu', '=', $id_menu)->first();
		$NmExport = $MsReport->NmExport;//dd($NmExport);

		if($NoPolis != ''){
			$file_export = $NmExport.'_'.$NoPolis.'_';
		}else{
			$file_export = $NmExport.'_ALL_';
		}//dd($file_export );

		if($tipe_file == 'csv'){
			$date = date('dmY');
			$fileName = $file_export . $date . '.csv';
			return Excel::download(new ExportExcel($request, $tipe_file), $fileName);
		}else{
			$date = date('dmY');
			$fileName = $file_export . $date . '.xls';
			return Excel::download(new ExportExcel($request, $tipe_file), $fileName);
		}
    }

	 public function get_kpa(Request $request, $id) {

            $select_kpa = $this->get_arr_combobox("SELECT KD_KANTOR,CONCAT(KD_KANTOR,'-',NAMA_KANTOR) AS NAMA_KANTOR FROM ODS_BLS.dbo.KANTOR WHERE KD_JENIS_KANTOR = '01' and KD_KANTOR_INDUK = '$id'", "KD_KANTOR", "NAMA_KANTOR", FALSE);
            $dataCbx = $this->arr2option($select_kpa, $request->selected);

        return $dataCbx;
    }

	 function arr2option($arrData, $selected) {
        $rtn = "";
		$rtn .= '<option  value="">--semua--</option>';
        foreach ($arrData as $k => $v) {
            $selected_id = $k == $selected ? 'selected' : '';
            $rtn .= '<option ' . $selected_id . ' value="' . $k . '">' . $v . '</option>';
        }
        return $rtn;
    }


}
