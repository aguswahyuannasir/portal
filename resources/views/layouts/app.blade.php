<!DOCTYPE html>

<html lang="en">
<!-- begin::Head -->

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />


<head>
    <meta charset="utf-8" />

    <title>Portal Beta</title>
    <meta name="description" content="Updates and statistics">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <script src="{{asset('assets-login')}}/ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Fonts -->


    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets-login')}}/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets-login')}}/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset('assets-login')}}/demo/default/skins/header/base/light.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets-login')}}/demo/default/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets-login')}}/demo/default/skins/brand/dark.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets-login')}}/demo/default/skins/aside/dark.css" rel="stylesheet" type="text/css" />
    <!--end::Layout Skins -->
    <link href="{{asset('css')}}/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <style>
            #fountainG{
                position: fixed;
               top: 50%;
               left: 50%;
            }

            .fountainG{
                position:absolute;
                top:0;
                background-color:rgb(0,0,0);
                width:28px;
                height:28px;
                animation-name:bounce_fountainG;
                    -o-animation-name:bounce_fountainG;
                    -ms-animation-name:bounce_fountainG;
                    -webkit-animation-name:bounce_fountainG;
                    -moz-animation-name:bounce_fountainG;
                animation-duration:1.5s;
                    -o-animation-duration:1.5s;
                    -ms-animation-duration:1.5s;
                    -webkit-animation-duration:1.5s;
                    -moz-animation-duration:1.5s;
                animation-iteration-count:infinite;
                    -o-animation-iteration-count:infinite;
                    -ms-animation-iteration-count:infinite;
                    -webkit-animation-iteration-count:infinite;
                    -moz-animation-iteration-count:infinite;
                animation-direction:normal;
                    -o-animation-direction:normal;
                    -ms-animation-direction:normal;
                    -webkit-animation-direction:normal;
                    -moz-animation-direction:normal;
                transform:scale(.3);
                    -o-transform:scale(.3);
                    -ms-transform:scale(.3);
                    -webkit-transform:scale(.3);
                    -moz-transform:scale(.3);
                border-radius:19px;
                    -o-border-radius:19px;
                    -ms-border-radius:19px;
                    -webkit-border-radius:19px;
                    -moz-border-radius:19px;
            }

            #fountainG_1{
                left:0;
                animation-delay:0.6s;
                    -o-animation-delay:0.6s;
                    -ms-animation-delay:0.6s;
                    -webkit-animation-delay:0.6s;
                    -moz-animation-delay:0.6s;
            }

            #fountainG_2{
                left:29px;
                animation-delay:0.75s;
                    -o-animation-delay:0.75s;
                    -ms-animation-delay:0.75s;
                    -webkit-animation-delay:0.75s;
                    -moz-animation-delay:0.75s;
            }

            #fountainG_3{
                left:58px;
                animation-delay:0.9s;
                    -o-animation-delay:0.9s;
                    -ms-animation-delay:0.9s;
                    -webkit-animation-delay:0.9s;
                    -moz-animation-delay:0.9s;
            }

            #fountainG_4{
                left:88px;
                animation-delay:1.05s;
                    -o-animation-delay:1.05s;
                    -ms-animation-delay:1.05s;
                    -webkit-animation-delay:1.05s;
                    -moz-animation-delay:1.05s;
            }

            #fountainG_5{
                left:117px;
                animation-delay:1.2s;
                    -o-animation-delay:1.2s;
                    -ms-animation-delay:1.2s;
                    -webkit-animation-delay:1.2s;
                    -moz-animation-delay:1.2s;
            }

            #fountainG_6{
                left:146px;
                animation-delay:1.35s;
                    -o-animation-delay:1.35s;
                    -ms-animation-delay:1.35s;
                    -webkit-animation-delay:1.35s;
                    -moz-animation-delay:1.35s;
            }

            #fountainG_7{
                left:175px;
                animation-delay:1.5s;
                    -o-animation-delay:1.5s;
                    -ms-animation-delay:1.5s;
                    -webkit-animation-delay:1.5s;
                    -moz-animation-delay:1.5s;
            }

            #fountainG_8{
                left:205px;
                animation-delay:1.64s;
                    -o-animation-delay:1.64s;
                    -ms-animation-delay:1.64s;
                    -webkit-animation-delay:1.64s;
                    -moz-animation-delay:1.64s;
            }



            @keyframes bounce_fountainG{
                0%{
                transform:scale(1);
                    background-color:rgb(0,0,0);
                }

                100%{
                transform:scale(.3);
                    background-color:rgb(255,255,255);
                }
            }

            @-o-keyframes bounce_fountainG{
                0%{
                -o-transform:scale(1);
                    background-color:rgb(0,0,0);
                }

                100%{
                -o-transform:scale(.3);
                    background-color:rgb(255,255,255);
                }
            }

            @-ms-keyframes bounce_fountainG{
                0%{
                -ms-transform:scale(1);
                    background-color:rgb(0,0,0);
                }

                100%{
                -ms-transform:scale(.3);
                    background-color:rgb(255,255,255);
                }
            }

            @-webkit-keyframes bounce_fountainG{
                0%{
                -webkit-transform:scale(1);
                    background-color:rgb(0,0,0);
                }

                100%{
                -webkit-transform:scale(.3);
                    background-color:rgb(255,255,255);
                }
            }

            @-moz-keyframes bounce_fountainG{
                0%{
                -moz-transform:scale(1);
                    background-color:rgb(0,0,0);
                }

                100%{
                -moz-transform:scale(.3);
                    background-color:rgb(255,255,255);
                }
            }

       .preloader {
           position: absolute;
           top: 0;
           left: 0;
           width: 100%;
           height: 100%;
           z-index: 9999;
           background-image: url('../giphy.gif');
           background-repeat: no-repeat;
           background-color: #FFF;
           background-position: center;
        }
    </style>

    <link rel="shortcut icon" href="{{asset('assets-login')}}/media/logos/favicon.ico" />
    <!-- Hotjar Tracking Code for keenthemes.com -->
    <script>
        (function(h, o, t, j, a, r) {
            h.hj = h.hj || function() {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 1070954,
                hjsv: 6
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37564768-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-37564768-1');
    </script>



    <!--begin::Global Theme Bundle(used by all pages) -->
    <script src="{{asset('assets-login')}}/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="{{asset('assets-login')}}/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <!--end::Global Theme Bundle -->


    <!--begin::Global App Bundle(used by all pages) -->
    <script src="{{asset('assets-login')}}/app/bundle/app.bundle.js" type="text/javascript"></script>
    <!--end::Global App Bundle -->

      <!--begin::Page Scripts(used by this page) -->
        <script src="{{asset('assets-login')}}/app/bundle/sweetalert2.js" type="text/javascript"></script>
    <!--end::Page Scripts -->

    <!--@yield('page-script','')-->


    <script src="{{asset('js')}}/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="{{asset('js')}}/dataTables.bootstrap4.min.js" type="text/javascript"></script>
    <script src="{{asset('js')}}/main.js" type="text/javascript"></script>


    <link href="{{asset('css')}}/adminlte.min.css" rel="stylesheet" type="text/css" />
    {{-- <script src="{{asset('js')}}/jquery-3.6.0.min.js"></script> --}}
    <script src="{{asset('js')}}/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/solid-gauge.js"></script>

</head>
<!-- end::Head -->

<!-- begin::Body -->

<body class="kt-header--fixed kt-header-mobile--fixed kt-subheader--fixed kt-subheader--enabled kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

    <!-- begin:: Page -->
    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
        <div class="kt-header-mobile__logo">
            <a href="index-2.html">
                <img alt="Logo" src="{{asset('assets-login')}}/media/logos/logo-light.png" />
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

            <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->
     @include('layouts.side')

    <!-- end:: Page -->
<!--
  <div class="preloader"></div>
 -->

    <div id="fountainG" class="page-loader" style="display:none;">
    <div id="fountainG_1" class="fountainG"></div>
    <div id="fountainG_2" class="fountainG"></div>
    <div id="fountainG_3" class="fountainG"></div>
    <div id="fountainG_4" class="fountainG"></div>
    <div id="fountainG_5" class="fountainG"></div>
    <div id="fountainG_6" class="fountainG"></div>
    <div id="fountainG_7" class="fountainG"></div>
    <div id="fountainG_8" class="fountainG"></div>
    </div>
    <!-- begin::Global Config(global config for global JS sciprts) -->
    <script>
        var KTAppOptions = {
            "colors": {
                "state": {
                    "brand": "#5d78ff",
                    "dark": "#282a3c",
                    "light": "#ffffff",
                    "primary": "#5867dd",
                    "success": "#34bfa3",
                    "info": "#36a3f7",
                    "warning": "#ffb822",
                    "danger": "#fd3995"
                },
                "base": {
                    "label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
                    "shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
                }
            }
        };
    </script>
    <!-- end::Global Config -->

    @stack('script')

</body>
<!-- end::Body -->

<!-- Mirrored from keenthemes.com/metronic/preview/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 07 Apr 2019 01:37:18 GMT -->

</html>
