<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;
Use DataTables;
use App\Http\Traits\ActMain;
use App\Http\Traits\Helper;

class ProductController extends Controller
{
    function index(Request $request)
    {
		$input = $request->all();
		$id_menu = $request->input('idmenu');
		$id_role = session('id_role');
		$kode_user = session('kode_user');

		$role = DB::table('role_menus')->select('*')->where('id_menu', '=', $id_menu)->where('id_role', '=', $id_role)->first();

        $Created = $role->created;
		if($Created != ""){
			$Created = $role->created;
		}else{
			$Created = 'none';
		}

      return view('product.index',['Created'=>$Created,'id_menu'=>$id_menu]);
    }

    function data(Request $request)
    {

        if(request()->ajax())
        {
            $idmenu = $request->idmenu;

            $data = DB::select("SELECT * FROM products");

            return Datatables::of($data)
                    ->addIndexColumn()
                        ->addColumn('action', function($row) use ($idmenu){
                        $btn = '<a onclick="add_action(\'user/update/'.$row->id_product.'/'.$idmenu.'\',\'_content_\',\'Master Client\');return false;" title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-edit"></i></a>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

    }
}
