<?php

namespace App\Http\Traits;

use DB,Session;;
use Carbon\Carbon;


trait ActMain {
	
	
	public function set_log($idmenu,$KdUser) {
       
       	$ip = $this->ip();
       	$device = $this->device();
       	$os = $this->os();
       	$browser = $this->browser();
       	$host = $_SERVER['REMOTE_ADDR'];		
		$TglLog = date('Y-m-d H:i:s');       
		$arrlog = [
            'TglLog' => $TglLog,
            'KdUser' => $KdUser,
            'KdMenu' => $idmenu,
            'IP' => $ip ,
            'HostName' => $host,
            'Browser' => $browser ,
            'Device' => $device,
            'OperatingSystem' => $os
        ];//dd($arrlog );
		
		$log = DB::table('MsWebLog')->insert($arrlog);
		
		return $log;
    }
	
	public static function os() { 
		$os_platform    =   "Unknown OS Platform";
		$os_array       =   array(
								'/windows nt 10/i'     =>   'Windows 10',
								'/windows nt 6.3/i'     =>  'Windows 8.1',
								'/windows nt 6.2/i'     =>  'Windows 8',
								'/windows nt 6.1/i'     =>  'Windows 7',
								'/windows nt 6.0/i'     =>  'Windows Vista',
								'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
								'/windows nt 5.1/i'     =>  'Windows XP',
								'/windows xp/i'         =>  'Windows XP',
								'/windows nt 5.0/i'     =>  'Windows 2000',
								'/windows me/i'         =>  'Windows ME',
								'/win98/i'              =>  'Windows 98',
								'/win95/i'              =>  'Windows 95',
								'/win16/i'              =>  'Windows 3.11',
								'/macintosh|mac os x/i' =>  'Mac OS X',
								'/mac_powerpc/i'        =>  'Mac OS 9',
								'/linux/i'              =>  'Linux',
								'/ubuntu/i'             =>  'Ubuntu',
								'/iphone/i'             =>  'iPhone',
								'/ipod/i'               =>  'iPod',
								'/ipad/i'               =>  'iPad',
								'/android/i'            =>  'Android',
								'/blackberry/i'         =>  'BlackBerry',
								'/webos/i'              =>  'Mobile'
							);
		foreach ($os_array as $regex => $value) { 
			if (preg_match($regex, $_SERVER["HTTP_USER_AGENT"])) {
				$os_platform    =   $value;
			}
		}   
		return $os_platform;
	}
	
	public static function device(){
		$ismobile = preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
		if($ismobile){
			$device = 'Mobile';
		}else{
			$device = 'Desktop';
		}
		
		return $device;
	}
	
	public static function browser() {
		$userAgent 		= (!empty($_SERVER['HTTP_USER_AGENT'])?$_SERVER['HTTP_USER_AGENT']:getenv('HTTP_USER_AGENT'));
		$host 			= $_SERVER['REMOTE_ADDR'];
		$uri 				= $_SERVER['REQUEST_URI'];
		$basic_browser 	= array (
									'Trident\/7.0' 	=> 'Internet Explorer 11',
									'Beamrise' 		=> 'Beamrise',
									'Opera' 		=> 'Opera',
									'OPR' 			=> 'Opera',
									'Shiira' 		=> 'Shiira',
									'Chimera' 		=> 'Chimera',
									'Phoenix' 		=> 'Phoenix',
									'Firebird' 		=> 'Firebird',
									'Camino' 		=> 'Camino',
									'Netscape' 		=> 'Netscape',
									'OmniWeb' 		=> 'OmniWeb',
									'Konqueror' 	=> 'Konqueror',
									'icab' 			=> 'iCab',
									'Lynx' 			=> 'Lynx',
									'Links' 		=> 'Links',
									'hotjava' 		=> 'HotJava',
									'amaya' 		=> 'Amaya',
									'IBrowse' 		=> 'IBrowse',
									'iTunes' 		=> 'iTunes',
									'Silk' 			=> 'Silk',
									'Dillo' 		=> 'Dillo', 
									'Maxthon' 		=> 'Maxthon',
									'Arora' 		=> 'Arora',
									'Galeon' 		=> 'Galeon',
									'Iceape' 		=> 'Iceape',
									'Iceweasel' 	=> 'Iceweasel',
									'Midori' 		=> 'Midori',
									'QupZilla' 		=> 'QupZilla',
									'Namoroka' 		=> 'Namoroka',
									'NetSurf' 		=> 'NetSurf',
									'BOLT' 			=> 'BOLT',
									'EudoraWeb' 	=> 'EudoraWeb',
									'shadowfox' 	=> 'ShadowFox',
									'Swiftfox' 		=> 'Swiftfox',
									'Uzbl' 			=> 'Uzbl',
									'UCBrowser' 	=> 'UCBrowser',
									'Kindle' 		=> 'Kindle',
									'wOSBrowser' 	=> 'wOSBrowser',
									'Epiphany' 	=> 'Epiphany', 
									'SeaMonkey' 	=> 'SeaMonkey',
									'Avant Browser' => 'Avant Browser',
									'Firefox' 		=> 'Firefox',
									'Chrome' 		=> 'Google Chrome',
									'MSIE' 			=> 'Internet Explorer',
									'Internet Explorer' => 'Internet Explorer',
									'Safari' 		=> 'Safari',
									'Mozilla' 		=> 'Mozilla'  
								);
		foreach($basic_browser as $pattern => $name) {
			if( preg_match("/".$pattern."/i",$userAgent, $match)) {
				$browser = $name.' ';
				$known = array('Version', $pattern, 'other');
				$pattern_version = '#(?<browser>' . join('|', $known).')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
				if (!preg_match_all($pattern_version, $userAgent, $matches)) {}
				
				$i = count($matches['browser']);
				if ($i != 1) {
					if (strripos($userAgent,"Version") < strripos($userAgent,$pattern)){
						$browser .= $matches['version'][0];
					}
					else {
						$browser .= $matches['version'][1];
					}
				}
				else {
					$browser .= $matches['version'][0];
				}
				break;
			}
		}
		return $browser;
   }
	
	public function ip() {
		if (getenv('HTTP_CLIENT_IP'))
			$ipAddress = getenv('HTTP_CLIENT_IP');
		else if(getenv('HTTP_X_FORWARDED_FOR'))
			$ipAddress = getenv('HTTP_X_FORWARDED_FOR');
		else if(getenv('HTTP_X_FORWARDED'))
			$ipAddress = getenv('HTTP_X_FORWARDED');
		else if(getenv('HTTP_FORWARDED_FOR'))
			$ipAddress = getenv('HTTP_FORWARDED_FOR');
		else if(getenv('HTTP_FORWARDED'))
			$ipAddress = getenv('HTTP_FORWARDED');
		else if(getenv('REMOTE_ADDR'))
			$ipAddress = getenv('REMOTE_ADDR');
		else
			$ipAddress = 'UNKNOWN';
		
		return $ipAddress;
	}
	
	
	
	
	public function encryptValue($value){
   $cipher = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
    $key256 = '1y4p0ZNA365m2DoNqRvdQlgXMqP8YeJn';
    $iv = 'e7wvP9KdWoQNM1Xa';
    
    @mcrypt_generic_init($cipher, $key256, $iv);
    $cipherText256 = @mcrypt_generic($cipher, $value);
    @mcrypt_generic_deinit($cipher);
    $encrypted = bin2hex($cipherText256);
    return $encrypted;
  }
 
  function decryptValue($value){
    $cipher = @mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
    $key256 = env('CRYPTO_KEY', '');
    $iv = env('CRYPTO_IV','');

    $limbo = hex2bin($value);
    @mcrypt_generic_init($cipher, $key256, $iv);
    $decrypted = @mdecrypt_generic($cipher, $limbo);
    return trim($decrypted);
  }
 

    public function get_arr_combobox($query, $key, $value, $empty = FALSE, $option = 0, &$disable = "") {
        $combobox = array();
        if ($option == 0) {
            $data = DB::select($query);
        } else {
            $data = DB::table('tm_ruangan')->where('status_ruangan', 1)->whereIn('id_ruangan', $option)->get();
        }

        if ($empty)
            $combobox[""] = "&nbsp;";
        if (!empty($data)) {
            // dd($data);
            $data = collect($data)->map(function($x) {
                return (array) $x;
            });
            $kodedis = "";
            $arrdis = array();
            foreach ($data as $row) {
                if (is_array($disable)) {
                    if ($kodedis == $row[$disable[0]]) {
                        $row_data = "";
                        if (!array_key_exists($row[$key], $combobox))
                            if (is_array($value)) {
                                foreach ($value as $kyy => $val) {

                                    $row_data .= $row[$val] . ' ' . $kyy . ' ';
                                }
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row_data;
                            } else {
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row[$value];
                            }
                    } else {
                        $row_data = "";
                        if (!array_key_exists($row[$disable[0]], $combobox))
                            $combobox[$row[$disable[0]]] = $row[$disable[1]];
                        if (!array_key_exists($row[$key], $combobox))
                            if (is_array($value)) {
                                foreach ($value as $kyy => $val) {
                                    $row_data .= $row[$val] . ' ' . $kyy . ' / ';
                                }
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row_data;
                            } else {
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row[$value];
                            }
                    }
                    $kodedis = $row[$disable[0]];
                    if (!in_array($kodedis, $arrdis))
                        $arrdis[] = $kodedis;
                }else {
                    $row_data = "";
                    if (is_array($value)) {
                        foreach ($value as $kyy => $val) {
                            $row_data .= $row[$val] . ' ' . $kyy . ' / ';
                        }
                        $combobox[$row[$key]] = str_replace("'", "\'", $row_data);
                    } else {
                        $combobox[$row[$key]] = str_replace("'", "\'", $row[$value]);
                    }
                }
            }
            $disable = $arrdis;
        }
        return $combobox;
    }

    public function get_arr_combobox_kontrak($query, $key, $value, $empty = FALSE, $option = 0, &$disable = "") {
        $combobox = array();
        if ($option == 0) {
            $data = DB::select($query);
        } else {
            $data = DB::table('T_NODAFTAR_KONTRAK A')
                    ->rightjoin('TX_KONTRAK B', 'B.NO_KONTRAK', '=', 'A.ID')
                    ->where('B.status_izin', '=', '02040588')
                    ->get();
            // dd($data);
        }

        if ($empty)
            $combobox[""] = "&nbsp;";
        if (!empty($data)) {
            // dd($data);
            $data = collect($data)->map(function($x) {
                return (array) $x;
            });
            $kodedis = "";
            $arrdis = array();
            foreach ($data as $row) {
                if (is_array($disable)) {
                    if ($kodedis == $row[$disable[0]]) {
                        $row_data = "";
                        if (!array_key_exists($row[$key], $combobox))
                            if (is_array($value)) {
                                foreach ($value as $val) {
                                    $row_data .= $row[$val] . ' ';
                                }
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row_data;
                            } else {
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row[$value];
                            }
                    } else {
                        $row_data = "";
                        if (!array_key_exists($row[$disable[0]], $combobox))
                            $combobox[$row[$disable[0]]] = $row[$disable[1]];
                        if (!array_key_exists($row[$key], $combobox))
                            if (is_array($value)) {
                                foreach ($value as $val) {
                                    $row_data .= $row[$val] . ' ';
                                }
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row_data;
                            } else {
                                $combobox[$row[$key]] = "&nbsp; &nbsp;&nbsp;&nbsp;" . $row[$value];
                            }
                    }
                    $kodedis = $row[$disable[0]];
                    if (!in_array($kodedis, $arrdis))
                        $arrdis[] = $kodedis;
                }else {
                    $row_data = "";
                    if (is_array($value)) {
                        foreach ($value as $val) {
                            if ($val == 'tgl_daftar') {
                                $row_data .= date('m', strtotime($row[$val])) . ' / ' . date('Y', strtotime($row[$val]));
                            } else {
                                $row_data .= $row[$val] . ' / ';
                            }
                        }
                        $combobox[$row[$key]] = str_replace("'", "\'", $row_data);
                    } else {
                        $combobox[$row[$key]] = str_replace("'", "\'", $row[$value]);
                    }
                }
            }
            $disable = $arrdis;
        }
        return $combobox;
    }


    function generate_kode() {
        $milliseconds = round(microtime(true) * 1000);
        $seconds = $milliseconds / 1000;

        $millisec = round($seconds - ($seconds >> 0), 3) * 1000;
        $angka = '01234567890';
        $date = date('YmdHis');

        $unik = $millisec . str_shuffle($angka) . $date;
        return substr(str_shuffle($unik), 0, 10);
    }

  

}

?>