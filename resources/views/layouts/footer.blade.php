<!-- begin:: Footer -->
<div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" style="background-image: url(assets-login/media/bg/bg-4.jpg);">
    <div class="kt-footer__copyright" style="color: white;font-weight: bold;">
        Version {{ $version }}  &nbsp;&copy;&nbsp;Corporation</a>
    </div>
    <div class="kt-footer__menu">

    </div>
</div>
<!-- end:: Footer -->
