<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id_menu');
            $table->integer('kode_menu')->nullable();
            $table->string('nama_menu')->nullable();
            $table->integer('kode_induk_menu')->nullable();
            $table->string('url')->nullable();
            $table->string('order_menu')->nullable();
            $table->string('seq_menu')->nullable();
            $table->string('class_menu')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('created')->nullable();
            $table->string('updated')->nullable();
            $table->string('download')->nullable();
            $table->string('approve')->nullable();
            $table->string('is_active')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
