<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Exceptions\HttpResponseException;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use DB, Validator, Cookie, Session, Redirect, Mail, Hash;;
use App\Custom;
use GuzzleHttp\Client;
use App\Http\Traits\ActMain;

class LoginController extends Controller
{
	use ActMain;
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
     /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login()
    {
        return view('login.login');
    }

    public function logout(){
		$kode_user = session('kode_user');

        Auth::logout();
        Session::flush();
        if(!(Auth::check())){
            return Redirect::to('/login');
        }
    }

    public function postLogin(Request $request)
    {

        $kode_user = $request->username;
        $password = $request->password;


		$user = DB::table('users')
                ->where('kode_user', $kode_user)
                ->first();
		if (!$user) {
			$rtn = "ER|NIK atau Kata Kunci salah!!";
            return $rtn;
		}

        // $hashedPassword = $user->password;

		$arrSession = [ 'id' => $user->id_user,
						'id_role' => $user->id_role,
						'kode_user' => $user->kode_user,
						'nama' => $user->name,
						'email' => $user->email,
						'is_active' => $user->is_active,
						'is_lock' => $user->is_lock
					];

		if($user != null){
			// if($user->IsActive == '1'){
			// 	if($user->IsLock == '0'){
			// 		if($user->IsLDAP == '1') {
			// 			try{
			// 			$LdapUriValParameter = $LdapUriValParameter->Value;
			// 			$client = new Client();
			// 					$response = $client->post($LdapUriValParameter, [
			// 						'json' => [
			// 							'username' => $this->encryptValue($email),
			// 							'password' => $this->encryptValue($request->password)
			// 						]
			// 					]);//	dd($response);

			// 					$log = $this->set_log('login',$KdUser);

			// 					if ($log) {
			// 						DB::commit();
			// 						DB::table('MsUser')->where('KdUser', $KdUser)->update(array('Attemps' => null));
			// 						session($arrSession);
			// 						$rtn = "OK|Login Berhasil|" . url('home') . "";
			// 						return $rtn;
			// 					}else{
			// 						$rtn = "ER|Login gagal.";
			// 						return $rtn;
			// 					}

			// 			}catch (\Exception $e) {
			// 					$rtn = "ER|Login gagal. NIK atau Kata Kunci salah.";
			// 					return $rtn;
			// 			}
			// 		}else{
			// 			if (Hash::check($password, $hashedPassword)) {
							// $log = $this->set_log('login',$KdUser);
							// if ($log) {
								// DB::commit();
								DB::table('users')->where('kode_user', $kode_user)->update(array('attemps' => null));
								session($arrSession);
								$rtn = "OK|Login Berhasil|" . url('home') . "";
								return $rtn;
					// 		}else{
					// 			$rtn = "ER|Login gagal.NIK atau Kata Kunci salah";
					// 			return $rtn;
					// 		}
					// 	} else {
					// 		#salah password update attemps
					// 		$attemps = $user->Attemps;
					// 		$parameter = 3;
					// 		if($attemps >= $parameter){
					// 			DB::table('MsUser')->where('KdUser', $KdUser)->update(array('IsLock' => '1'));
					// 			$rtn = "ER|Login gagal. NIK atau Kata Kunci salah.";
					// 			return $rtn;
					// 		}else{
					// 			$nilai = 1;
					// 			$count = $attemps + $nilai;
					// 			DB::table('MsUser')->where('KdUser', $KdUser)->update(array('Attemps' => $count));
					// 			$rtn = "ER|Login gagal. NIK atau Kata Kunci salah.";
					// 			return $rtn;
					// 		}
					// 	}
					// }
			// 	}else{
			// 		$rtn = "ER|NIK Anda di Lock.";
			// 		return $rtn;
			// 	}
			// }else{
			// 	$rtn = "ER|NIK Tidak Aktif.";
			// 	return $rtn;
			// }

		}else{
			$rtn = "ER|NIK atau Kata Kunci salah!!";
            return $rtn;
		}



    }
}
