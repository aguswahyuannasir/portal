function toko(chartName,result){
    crt =   {
                options : {
                    chart: {
                        zoomType: 'xy',
                        backgroundColor:'transparent'
                    },
                    title: {
                        text: 'Toko'
                    },
                    subtitle: {
                        text: 'Source: SummaryDashboard'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct'
                        ],
                        crosshair: true
                    },
                    yAxis: [{ // Primary yAxis
                        labels: {
                            format: '{value}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Polis',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, { // Secondary yAxis
                        title: {
                            text: 'Agen Menutup',
                            style: {
                                color:'#FF1733',
                            }
                        },
                        labels: {
                            format: '{value}',
                            style: {
                                color: '#FF1733',
                            }
                        },
                        opposite: true
                    }],
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        color: '#FACB29',
                        name: 'Polis',
                        type: 'column',
                        yAxis: 1,
                        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1],
                    }, {
                        color: '#FF1733',
                        name: 'Agen Menutup',
                        type: 'line',
                        data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3],
                    }]
                },

                generateChart: function(chartName,result) {

                    crt.options.xAxis.categories = result.kategori;
                    crt.options.series[0].data = result.Polis;
                    crt.options.series[1].data = result.AgenMenutup;

                    Highcharts.chart(chartName, crt.options);
                }

            };

    crt.generateChart(chartName,result);
}
