<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
return view('login.login');
});

//Auth::routes();

//Route::group(['middleware' => 'User_Role'], function () {
//home
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@home')->name('home');
Route::post('/home/chart_product', 'HomeController@chart_product');
Route::post('/home/chart_materi', 'HomeController@chart_materi');
Route::post('/home/chart_toko', 'HomeController@chart_toko');


//transaksi
Route::post('/transaksi', 'TransaksiController@index')->name('transaksi.index');
Route::get('/transaksi/data', 'TransaksiController@data');
//product
Route::post('/product', 'ProductController@index')->name('product.index');
Route::get('/product/data', 'ProductController@data');
//product
Route::post('/materi', 'MateriController@index')->name('materi.index');
Route::get('/materi/data', 'MateriController@data');



Route::post('/menu', 'MenuController@index')->name('index');
Route::get('/menu/data', 'MenuController@data');


Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@postLogin');
Route::get('/logout', 'LoginController@logout');


//MENU MANAGEMENT ROUTE
Route::post('/menu_management', 'MenuManagementController@index')->name('menu.index');
Route::get('/menu_management/data', 'MenuManagementController@data');
Route::post('/menu_management/create/', 'MenuManagementController@create')->name('create');
Route::post('/menu_management/update/{id}', 'MenuManagementController@update')->name('update');
Route::post('/menu_management/add_data', 'MenuManagementController@add_data')->name('add_data');
Route::post('/menu_management/edit_data', 'MenuManagementController@edit_data')->name('edit_data');
Route::post('/menu_management/delete/{id}', 'MenuManagementController@delete')->name('delete');


//MENU ROLE ROUTE
Route::post('/menu_role', 'MenuRoleController@index')->name('menu_role.index');
Route::get('/menu_role/data', 'MenuRoleController@data');
Route::post('/menu_role/create/{id}', 'MenuRoleController@create')->name('menu_role.create');
Route::post('/menu_role/update/{id}/{idmenu}', 'MenuRoleController@update')->name('menu_role.update');
Route::post('/menu_role/add_data', 'MenuRoleController@add_data')->name('menu_role.add_data');
Route::post('/menu_role/edit_data', 'MenuRoleController@edit_data')->name('menu_role.edit_data');
Route::post('/menu_role/delete/{id}', 'MenuRoleController@delete')->name('menu_role.delete');
Route::post('/menu_role/export_excel', 'MenuRoleController@export_excel')->name('menu_role.export_excel');
Route::get('/menu_role/export_excel', 'MenuRoleController@export_excel')->name('menu_role.export_excel');

//USER MANAGEMENT ROUTE
Route::post('/user', 'UserController@index')->name('index');
Route::get('/user/data', 'UserController@data');
Route::post('/user/create/{id}', 'UserController@create')->name('create');
Route::post('/user/update/{id}/{idmenu}', 'UserController@update')->name('update');
Route::post('/user/add_data', 'UserController@add_data')->name('add_data');
Route::post('/user/edit_data', 'UserController@edit_data')->name('edit_data');
Route::post('/user/delete/{id}', 'UserController@delete')->name('delete');
Route::get('/user/export_excel', 'UserController@export_excel')->name('user_export');
Route::post('/user/import_excel', 'UserController@import_excel')->name('import_excel');
Route::post('/user/ceKdUser', 'UserController@ceKdUser')->name('ceKdUser');
Route::post('/user/cekEmail', 'UserController@cekEmail')->name('cekEmail');


//Report ROUTE
Route::post('/report', 'ReportController@index')->name('menu.index');
Route::get('/report/data', 'ReportController@data');
Route::post('/report/export_excel', 'ReportController@export_excel')->name('report.export_excel');
Route::post('/report/get_kpa/{id}', 'ReportController@get_kpa')->name('report.get_kpa');










