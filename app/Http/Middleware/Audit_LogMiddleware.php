<?php 
namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;
use Closure;

class Audit_LogMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = implode($request->segments(), '/');
        $url_segments = preg_replace('/[^A-Za-z0-9 \.\,\-\|\*\;\/\&\#\(\)\_]/', '', $url);        
        
        $result = DB::select("SELECT * FROM m_url_segments WHERE URL_SEGMENTS = SUBSTRING_INDEX('" . $url_segments . "', '/', ROUND((LENGTH(URL_SEGMENTS)-LENGTH(REPLACE(URL_SEGMENTS,'/','')))/LENGTH('/')+1)) AND KEY_MATCH_VALUES IS NOT NULL ORDER BY KEY_MATCH_VALUES DESC");
        $url_segments_info = $this->get_url_segments_info($request, $result);
        // dd($request->all());

        if(sizeof($url_segments_info) > 0)
            $this->log($request, $url_segments_info);

        return $next($request);
    }

    public function get_url_segments_info($request, $results)
    {
        $matches = 0;
        for($i = 0; $i < sizeof($results); $i++)
        {
            $key_match_values = json_decode($results[$i]->KEY_MATCH_VALUES);
            $match_values = (array)$key_match_values->match_values;

            foreach ($match_values as $match_values_key => $match_values_val)
            {
                if($request->{$match_values_key} == $match_values_val)
                    $matches++;
            }

            if(sizeof($match_values) > 0)
            {
                if($matches == sizeof($match_values))
                    return $results[$i];
            }
            else
                return $results[$i];
        }
    }

    public function log($request, $url_segments_info)
    {
        $t_activity_log["WEB_SERVER_NAME"] = $request->server('SERVER_NAME');
        $t_activity_log["USER_ID"] = session("USERID");
        $t_activity_log["SESSION_ID"] = session()->getId();
        $t_activity_log["REQUEST_METHOD"] = $request->method();
        $t_activity_log["URL_SEGMENTS_ID"] = $url_segments_info->URL_SEGMENTS_ID;
        $t_activity_log["REQUEST_PAYLOAD"] = json_encode($request->all(), TRUE);
        $t_activity_log["CLIENT_IP_ADDRESS"] = $request->ip();
        $t_activity_log["CLIENT_HTTP_AGENT"] = $request->header('User-Agent');

        $exec = $this->insertreference("t_activity_log", $t_activity_log);

        if ($exec)
        {
            $identifier_types = (array)json_decode($url_segments_info->IDENTIFIER_TYPES)->id_types;
            for ($i=0; $i < sizeof($identifier_types); $i++)
            {
                if (strpos($identifier_types[$i], '['))
                {
                    $var_arr_name = substr($identifier_types[$i], 0, strpos($identifier_types[$i], '['));
                    $var_arr_key = str_replace("'", "", substr($identifier_types[$i], strpos($identifier_types[$i], '[') + 1, strpos($identifier_types[$i], ']') - strpos($identifier_types[$i], '[') - 1));
                    $identifier_types_values[$var_arr_key] = $request->{$var_arr_name}[$var_arr_key];
                }
                else
                    $identifier_types_values[$identifier_types[$i]] = $request->{$identifier_types[$i]};
            }

            $db_t_activity_log = DB::select("SELECT * FROM t_activity_log WHERE USER_ID = '" . session("USERID") . "' AND SESSION_ID = '" . session()->getId() . "' AND CLIENT_IP_ADDRESS = '" . $request->ip() . "' ORDER BY START_TIME DESC LIMIT 1");
            $t_activity_details_log["ACTIVITY_LOG_ID"] = $db_t_activity_log[0]->ACTIVITY_LOG_ID;
            $t_activity_details_log["IDENTIFIER_TYPES_VALUE"] = json_encode($identifier_types_values, TRUE);
            $t_activity_details_log["TASK"] = $this->construct_task($identifier_types_values, $url_segments_info->MESSAGE_FORMAT);
            
            if(strlen($t_activity_details_log["TASK"]) > 0)
                $exec = $this->insertreference("t_activity_details_log", $t_activity_details_log);

        }
    }
    
    function construct_task($identifier_types_values, $task_format = "")
    {
        $message = $task_format;

        foreach ($identifier_types_values as $key => $value)
        {
            if(is_array($value))
                $value = implode(', ', $value);
            $message = str_replace('{' . $key . '}', $value, $message);
        }

        return $message;
    }

    function insertreference($table, $arrData, $allField = false)
    {
        DB::statement("SET sql_mode = ''");
        $arrData = array_change_key_case($arrData,CASE_UPPER);
        $arrData = array_map('trim', $arrData);
        //$arrData = array_map('strtoupper', $arrData);
        $sqlTBL = 'SHOW FIELDS FROM ' . $table;
        $arrTBL = DB::select($sqlTBL);
        $whrTBL = '';
        $data   = '';
        foreach ($arrTBL as $aTBL) {
            $aTBL = array_change_key_case((array)$aTBL, CASE_UPPER);
            //$aTBL = array_map('strtoupper' , $aTBL);
            $trueData = false;
            if ($aTBL['EXTRA'] != 'AUTO_INCREMENT') {
                if($allField){
                    $data[$aTBL['FIELD']] = $arrData[$aTBL['FIELD']];
                    $trueData = true;
                }else{
                    if(isset($arrData[$aTBL['FIELD']])){
                        $data[$aTBL['FIELD']] = $arrData[$aTBL['FIELD']];
                        $trueData = true;
                    }
                }
                if($trueData){
                    $type = explode('(', $aTBL['TYPE']);
                    switch ($type[0]){
                        case'DATE':
                            $date = explode('-', $arrData[$aTBL['FIELD']]);
                            if(checkdate($date[1], $date[0], $date[2]))
                            { //month-date-year
                                $data[$aTBL['FIELD']] = $date[2] . '-' . $date[1] . '-' . $date[0]; //year-month-date
                            }else{
                                $data[$aTBL['FIELD']] = DB::raw("NULL");
                            }
                        break;
                        case'DOUBLE':
                        case'INT':
                        case'SMALLINT':
                            $data[$aTBL['FIELD']] = str_replace(',', '', $arrData[$aTBL['FIELD']]);
                            $data[$aTBL['FIELD']] = (strlen(trim($data[$aTBL['FIELD']])) > 0 ? $data[$aTBL['FIELD']] : DB::raw("0"));
                            $data[$aTBL['FIELD']] = (substr_count($data[$aTBL['FIELD']], ".") > 1 ? substr($data[$aTBL['FIELD']], 0, strpos($data[$aTBL['FIELD']], '.', 2)) : $data[$aTBL['FIELD']]);
                        case'BIT':
                            $data[$aTBL['FIELD']] = DB::raw($data[$aTBL['FIELD']]);
                        break;
                    }
                }
            }
            #find key
            if ($aTBL['KEY'] == 'PRI') {
                $whrTBL[$aTBL['FIELD']] = $arrData[$aTBL['FIELD']];//$data[$aTBL['FIELD']];
                if($trueData){
                    $whrTBL[$aTBL['FIELD']] = $data[$aTBL['FIELD']];//$data[$aTBL['FIELD']];
                }
            }
        }
        if($whrTBL);
        $datCEK = DB::table($table)->where($whrTBL)->count();
        if ($datCEK == 0) {
            $exec = DB::table($table)->insert($data);
        } else {
            $exec = DB::table($table)->where($whrTBL)->update($data);
        }
        return $exec;
    }
}