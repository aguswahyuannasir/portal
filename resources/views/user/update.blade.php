<script src="{{ asset('js/modal.js') }}"></script>
<script src="{{ asset('js/claim.js') }}"></script>


<div class="kt-portlet" style="margin-top: 30px;">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
               Manajemen User  (Ubah)
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <!-- <form name="fuser" id="fuser" class="kt-form kt-form--label-right" action="user/add_data" method="post" onsubmit="return false;">  -->
    <form name="fuser" id="fuser" novalidate="novalidate" class="kt-form kt-form--label-right" action="{{'user/edit_data'}}" method="post" onsubmit="return false;"> 


        @csrf
        <input type="hidden" name="act" id="act" value="update" onchange="act();" readonly="readonly">
        <input type="hidden" name="IdMsUser" id="IdMsUser" value="{{ old('IdMsUser', isset($user) ? $user->IdMsUser : '') }}">		
		<input name="idmenu" id="idmenu" type="hidden" value="{{$idmenu}}">
        <div class="kt-portlet__body">
         
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">NIK</label>
                        <div class="col-10">
                            <input type="text" disabled name="user[KdUser]" onblur="ceKdUser(this.value)" id="KdUser" class="form-control" value="{{ old('KdUser', isset($user) ? $user->KdUser : '') }}">
                        </div>
                    </div>
                     <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">LDAP</label>
                        <div class="col-10">
                            {!! $ldap !!}
                        </div>
                    </div>                 
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">User Role</label>
                        <div class="col-10">                    
                            {!! $userrole !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                   
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Nama user</label>
                        <div class="col-10">
                            <input type="text" name="user[NmUser]" id="NmUser" class="form-control"  value="{{ old('NmUser', isset($user) ? $user->NmUser : '') }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Email</label>
                        <div class="col-10">
                            <input type="text" disabled  name="user[Email]" onblur="cekEmail(this.value)" id="Email" class="form-control" placeholder="Silahkan masukkan email tanpa @"  value="{{ old('Email', isset($user) ? $user->Email : '') }}">
                        </div>
                    </div>
					  <div class="form-group row">
                        <label for="example-text-input" class="col-2 col-form-label">Status</label>
                        <div class="col-10">                            
                            {!! $status !!}
                        </div>
                    </div>
                 				             
                </div>
          </div>

        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button onclick="CekForm()"  class="btn btn-success">Simpan</button>
                        <button type="reset" onclick="call('user','_content_','USER MANAGEMENT',{{$idmenu}})" class="btn btn-secondary">Cancel</button>
                    </div>
                    
                </div>
            </div>
        </div>

    </form>
</div>
<script type="text/javascript">
  $(document).ready(function() {
        $('.select2').select2();
        
        //JS For Import Excel-->(JSON)HTML
        $("#fileUploader").change(function(evt){
            var selectedFile = evt.target.files[0];
            var reader = new FileReader();
            reader.onload = function(event) {
                var data = event.target.result;
                var workbook = XLSX.read(data, {
                    type: 'binary'
                });
                workbook.SheetNames.forEach(function(sheetName) {                     
                    var XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
                    var json_object = JSON.stringify(XL_row_object);
                    getTable(json_object);
                });
            };
            reader.onerror = function(event) {
                console.error("File could not be read! Code " + event.target.error.code);
            };
            reader.readAsBinaryString(selectedFile);
        });

  });
  
   function CekForm(){//alert(val);
        var KdUser = document.getElementById("KdUser").value;
		if(KdUser == ""){
			//document.getElementById("KdUser").focus();
			Swal.fire('Error', 'Silahkan Input NIK', 'error');
		}else{
			save_post_msg_update('fuser');
		}     
        
  }

  function ceKdUser(val){//alert(val);
        var url = "/user/ceKdUser";
       
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: {val : val},
            success: function(res){
				if(res == 'NO'){
					document.getElementById("KdUser").value = "";
					Swal.fire('Error', 'NIK Sudah Ada', 'error');
				}               
            },
            error: function(){
                Swal.fire('Error', 'EROR', 'error');
            }
        })
  }
  
  function cekEmail(val){//alert(val);
        var url = "/user/cekEmail";
       
        $.ajax({
            url: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            data: {val : val},
            success: function(res){
				if(res == 'NO'){
					document.getElementById("Email").value = "";
					Swal.fire('Error', 'Email Sudah Ada', 'error');
				}               
            },
            error: function(){
                Swal.fire('Error', 'EROR', 'error');
            }
        })
  }

</script>