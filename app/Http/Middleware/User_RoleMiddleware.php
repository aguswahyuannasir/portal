<?php 
namespace App\Http\Middleware;
use Illuminate\Support\Facades\DB;
use Closure;
use Redirect;

class User_RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        if(!session('role')){
            return Redirect::to('/login');
        }

        $url = implode($request->segments(), '/');
        $url_segments = preg_replace('/[^A-Za-z0-9 \.\,\-\|\*\;\/\&\#\(\)\_]/', '', $url);
        //$data = $request->session()->all();
         //dd($url_segments);
        // dd($request->act);
        $addtlparams="";
        if(trim(preg_replace('/\s+/', ' ', $request->act)) != "")
        // if($request->act != "")
        {
            $addtlparams = " AND INSTR(ADDITIONAL_PARAMETERS, '" . $request->act . "') > 0 ";
        }
        
        $is_granted = '';

        $result = DB::table('tm_menu_user as a')
                ->leftjoin('tm_menu as c', 'a.menu_id', '=', 'c.menu_id')
                 ->select('c.menu_id', 'c.menu_name', 'c.menu_class', 'c.menu_bg', 'c.menu_url', 'c.menu_seq')
                ->where('a.role_id', session('role'))
                ->where('a.user_id', '1')
                ->where('c.menu_url', $url_segments)
                ->groupby('c.menu_seq','c.menu_id', 'c.menu_name', 'c.menu_class', 'c.menu_bg', 'c.menu_url', 'c.menu_order')
                ->first();

   // dd(session()->all());
        // $role = session('role');
        //   $user_id = '1';
//         $result = DB::select("SELECT c.menu_id, c.menu_name, c.menu_class, c.menu_bg, c.menu_url, c.menu_seq
//                     FROM tm_menu_user a
//                     LEFT JOIN tm_menu_old c ON c.menu_id = a.menu_id
//                     WHERE a.user_id = ".$user_id." 
//                     AND c.menu_active = '1' 
//                     and a.role_id = '".$role."' and c.menu_url = '".$url_segments."'
// GROUP by c.menu_seq, c.menu_id, c.menu_name, c.menu_class, c.menu_bg, c.menu_url, a.menu_order
// order by   c.menu_bg, c.menu_bg, a.menu_order, c.menu_order asc" );
         //  dd($result);
        
       // $cek_email = DB::table('t_user')->where('email', 'mahmudialfin@gmail.com')->first();

        $is_granted = $result->menu_name;
       // dd($is_granted);
        if($is_granted){
            return $next($request);
        }
        else{
            dd('Sorry, you are not granted to this page.');
        }
    }
}