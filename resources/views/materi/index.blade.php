<div class="kt-portlet kt-portlet--mobile" style="margin-top: 50px">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
            <h3 class="kt-portlet__head-title">
				Materi
			</h3>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="table-responsive" style="margin-top: 15px">
			<table class="table table-bordered table-striped" id="order_table">
				<thead class="thead">
					<tr>
						<th>No.</th>
						<th>Field</th>
						<th>Field</th>
						<th>Field</th>
                        <th>Field</th>
                        <th>Field</th>
                        <th>Field</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function() {
            $('.input-daterange').datepicker({
                todayBtn: 'linked',
                format: 'yyyy-mm-dd',
                autoclose: true
            });


            load_data();

            function load_data(from_date = '', to_date = '') {
                $('#order_table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: 'product/data',
                        data: {
                            idmenu: {{$id_menu}}
                        }
                    },
                    columns: [{data: 'DT_RowIndex', orderable: false, searchable: false
						},{
                            data: 'id_product',
                            name: 'id_product'
                        }, {
                            data: 'field_1',
                            name: 'field_1'
                        }, {
                            data: 'field_2',
                            name: 'field_2'
                        }, {
                            data: 'field_3',
                            name: 'field_3'
                        }, {
                            data: 'field_4',
                            name: 'field_4'
                        }, {
                            data: 'field_5',
                            name: 'field_5'
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            }

        });


    </script>
