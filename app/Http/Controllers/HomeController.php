<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB,Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $id_role = session('id_role');

		if($id_role != ""){
			try{
				 $menu = DB::select("SELECT
                                        b.id_menu,
                                        b.kode_menu menu_id,
                                        b.nama_menu menu_name,
                                        b.class_menu menu_class,
                                        b.kode_induk_menu menu_main,
                                        b.url menu_url,
                                        b.order_menu,
                                        b.seq_menu menu_seq
                                    FROM
                                        role_menus a
                                        LEFT JOIN menus b ON b.id_menu = a.id_menu
                                    WHERE
                                        b.is_active = '1'
                                        AND a.id_role = '1'
                                    GROUP BY
                                        b.id_menu,
                                        b.kode_menu,
                                        b.nama_menu,
                                        b.class_menu,
                                        b.kode_induk_menu,
                                        b.url,
                                        b.order_menu,
                                        b.seq_menu
                                    ORDER BY
                                        b.kode_menu,
                                        b.seq_menu ASC" );

				foreach ($menu as $arrRow) {
					 $arrResult[$arrRow->menu_id] = array($arrRow->menu_name,  $arrRow->menu_class ,  $arrRow->menu_main ,  $arrRow->menu_url ,   $arrRow->menu_seq , $arrRow->id_menu );
				}


				$version = "beta";

				$viewData = array(
							'menu' => $menu,
							'arrResult' => $arrResult,
							'version' => $version,
							'content' => $this->home()
						);

			}catch (Throwable  $e) {
					 return Redirect::to('/login');
			}

			return view('layouts.app', $viewData);
		}else{
			 return Redirect::to('/login');
		}
    }

	public function home() {
        return view('home.index');
    }


    public function chart_product(){
        $sql  = DB::table('SummaryDashboard')
                ->selectRaw("NmKPR,
                            SUM(AmtPTPSin) AS sin,
                            SUM(AmtPTPReg) AS reg,
                            SUM(AmtPTPSin) + SUM(AmtPTPReg) AS total,
                            SUM(Polis) AS Polis,
                            SUM(AgenMenutup) AS AgenMenutup,
                            SUM(AgenBaru) AS AgenBaru
                            ")
                ->groupBy('NmKPR')
                ->orderBy('NmKPR', 'ASC')
                ->get();

        foreach($sql as $ls){

          $kategori[]     = $ls->NmKPR;
          $sin[]          = $ls->sin / 1000000;
          $reg[]          = $ls->reg / 1000000;
          $total[]        = $ls->total / 1000000;
          $Polis[]        = $ls->Polis / 1;
          $AgenMenutup[]  = $ls->AgenMenutup / 1;
          $AgenBaru[]     = $ls->AgenBaru / 1;

        }

        $respone['kategori']    = $kategori;
        $respone['sin']         = $sin;
        $respone['reg']         = $reg;
        $respone['total']       = $total;
        $respone['Polis']       = $Polis;
        $respone['AgenMenutup'] = $AgenMenutup;
        $respone['AgenBaru']    = $AgenBaru;

        return $respone;
    }

    public function chart_materi(){
        $sql  = DB::table('SummaryDashboard')
                ->selectRaw("NmKPR,
                            SUM(AmtPTPSin) AS sin,
                            SUM(AmtPTPReg) AS reg,
                            SUM(AmtPTPSin) + SUM(AmtPTPReg) AS total,
                            SUM(Polis) AS Polis,
                            SUM(AgenMenutup) AS AgenMenutup,
                            SUM(AgenBaru) AS AgenBaru
                            ")
                ->groupBy('NmKPR')
                ->orderBy('NmKPR', 'ASC')
                ->get();

        foreach($sql as $ls){

          $kategori[]     = $ls->NmKPR;
          $sin[]          = $ls->sin / 1000000;
          $reg[]          = $ls->reg / 1000000;
          $total[]        = $ls->total / 1000000;
          $Polis[]        = $ls->Polis / 1;
          $AgenMenutup[]  = $ls->AgenMenutup / 1;
          $AgenBaru[]     = $ls->AgenBaru / 1;

        }

        $respone['kategori']    = $kategori;
        $respone['sin']         = $sin;
        $respone['reg']         = $reg;
        $respone['total']       = $total;
        $respone['Polis']       = $Polis;
        $respone['AgenMenutup'] = $AgenMenutup;
        $respone['AgenBaru']    = $AgenBaru;

        return $respone;
    }

    public function chart_toko(){
        $sql  = DB::table('SummaryDashboard')
                ->selectRaw("NmKPR,
                            SUM(AmtPTPSin) AS sin,
                            SUM(AmtPTPReg) AS reg,
                            SUM(AmtPTPSin) + SUM(AmtPTPReg) AS total,
                            SUM(Polis) AS Polis,
                            SUM(AgenMenutup) AS AgenMenutup,
                            SUM(AgenBaru) AS AgenBaru
                            ")
                ->groupBy('NmKPR')
                ->orderBy('NmKPR', 'ASC')
                ->get();

        foreach($sql as $ls){

          $kategori[]     = $ls->NmKPR;
          $sin[]          = $ls->sin / 1000000;
          $reg[]          = $ls->reg / 1000000;
          $total[]        = $ls->total / 1000000;
          $Polis[]        = $ls->Polis / 1;
          $AgenMenutup[]  = $ls->AgenMenutup / 1;
          $AgenBaru[]     = $ls->AgenBaru / 1;

        }

        $respone['kategori']    = $kategori;
        $respone['sin']         = $sin;
        $respone['reg']         = $reg;
        $respone['total']       = $total;
        $respone['Polis']       = $Polis;
        $respone['AgenMenutup'] = $AgenMenutup;
        $respone['AgenBaru']    = $AgenBaru;

        return $respone;
    }

}
