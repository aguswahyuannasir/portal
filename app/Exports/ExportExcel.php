<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use DB;

class ExportExcel implements FromCollection, WithHeadings, ShouldAutoSize, WithColumnFormatting, WithEvents, WithCustomCsvSettings
{
	private $request_form;
	private $tipeFile;
	
    public function __construct($request, $tipeFile)
    {
		$this->request_form = $request;
		$this->tipeFile = $tipeFile;
    }

    public function collection()
    {
    	
		$arrfield  = array();
		$request = $this->request_form;	
		$menu_id = $request['menu_id'];
		$KdUser = session('KdUser');
		$to_remove = array("_token", "order_table_length","menu_id","tipe_file");
		$result = array_diff_key($request, array_flip($to_remove));//dd($result);
		foreach ($result as $key => $value) {			
			$arrfield[$key]  = $value==NULL?'ALL':$value;			
		}		
		$json = json_encode($arrfield);//dd($json);
		
		$MsReport = DB::table('MsReport')->select('*')->where('IdMsMenu', '=', $menu_id)->first();
		
		$Query = $MsReport->Query;
		$IdMsReport = $MsReport->IdMsReport;
		$explode = explode(';', $Query);
		$exp1 = $explode[0];
		$exp2 = $explode[1];
		$result = str_replace("@json","'$json'",$exp1);				
		
		DB::update($result);	
		
		$select = explode('#', $exp2);
		$select1 = $select[0];
		$select2 = $select[1];
		$query2 = DB::table($select1)->where('KdUser', $KdUser)->paginate(45000);//dd($query2);
		
		
		$header = DB::select("SELECT IdMsHeaderReport, IdMsReport, TypeReport, Label, DataField, DataType, Format, IsActive 
								FROM MsHeaderReport where IdMsReport = '".$IdMsReport."' and TypeReport = '2'" );//dd($header);
		
		$array  = array();	
		$kelauar  = [];	
		foreach ($header as $hasil) {		
				$array[] = $hasil->DataField;									
				$data_format[] = $hasil->DataType;
				$style_format[] = $hasil->Format;
		}			
		
		$output =  array();
        foreach ($query2 as $val){	//dd($param);
				foreach ($array as $key => $value){
					if($data_format[$key] == 'date'){
						if($val->$value != ""){
							$kelauar[$key] = date($style_format[$key], strtotime($val->$value) );
						}else{
							$kelauar[$key] = $val->$value;
						}
					}else{
						$kelauar[$key] = $val->$value;		
					}	
					
				}
				$output[] = $kelauar;
        }//dd($output);
       return collect($output);
    }
	
	

    public function headings(): array
    {	
		$request = $this->request_form;		
		$menu_id = $request['menu_id'];
		
		$MsReport = DB::table('MsReport')->select('*')->where('IdMsMenu', '=', $menu_id)->first();
		$IdMsReport = $MsReport->IdMsReport;
		
		$header = DB::select("SELECT IdMsHeaderReport, IdMsReport, TypeReport, Label, DataField, DataType, Format, IsActive 
			     FROM MsHeaderReport where IdMsReport = '".$IdMsReport."' and TypeReport = '2'" );//dd($header);
								
			
		$head_table = 0;
		foreach ($header as $key => $value) {	
			$fields[0][$head_table] = $value->Label;
			$head_table ++; 			
		}	//dd($fields);
		 return $fields;
    }
	
	public function columnFormats(): array
    {
		$request = $this->request_form;		
		$menu_id = $request['menu_id'];
		
		$MsReport = DB::table('MsReport')->select('*')->where('IdMsMenu', '=', $menu_id)->first();
		$IdMsReport = $MsReport->IdMsReport;
		
		$header = DB::select("SELECT IdMsHeaderReport, IdMsReport, TypeReport, Label, DataField, DataType, Format, IsActive 
			     FROM MsHeaderReport where IdMsReport = '".$IdMsReport."' and TypeReport = '2'" );//dd($header);
				 
		 $col = 'A'; 
		 $head_table = 0;
		foreach ($header as $key => $value) {	
			$DataType = $value->DataType;
			$Format = $value->Format;
			if($DataType == 'integer'){
				$fields[$col] = $Format;
			}elseif($DataType == 'number'){
				$fields[$col] = $Format; 
			}elseif($DataType == 'date'){
				$fields[$col] = 'dd/mm/yyyy';
			}elseif($DataType == 'text'){
				$fields[$col] = $Format;
			}else{
				$fields[$col] = '@';
			}
			$head_table ++; 			
			$col ++; 			
		}	//dd($fields);
         return $fields;
		
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:D1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(11);
            },
        ];
	}
	
	public function getCsvSettings(): array
    {
		$delimiter = ($this->tipeFile === 'csv') ? '|' : ',';
        return [
			'delimiter' => $delimiter
        ];
    }
}
