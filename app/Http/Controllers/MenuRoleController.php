<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB,Excel;
Use DataTables;
use App\Exports\ExportExcel;
use App\Http\Traits\ActMain;
use App\Http\Traits\Helper;

class MenuRoleController extends Controller
{
	use ActMain;
    use Helper;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    function index(Request $request)
    {
		$input = $request->all();
		$id_menu = $request->input('idmenu');
		$id_role = session('id_role');
		$kode_user = session('kode_user');

		$role = DB::table('role_menus')->select('*')->where('id_menu', '=', $id_menu)->where('id_role', '=', $id_role)->first();

        $Created = $role->created;
		if($Created != ""){
			$Created = $role->created;
		}else{
			$Created = 'none';
		}


      return view('menu_role.index',['Created'=>$Created,'id_menu'=>$id_menu]);
    }

    function data(Request $request)
    {

        if(request()->ajax())
        {
            $idmenu = $request->idmenu;

            $data = DB::select("SELECT
                                    A.id_role,
                                    A.nama_role,
                                    B.ind AS is_active
                                FROM
                                    roles A
                                    INNER JOIN reffs B ON B.reff = 'Aktif'
                                    AND B.code = A.is_active
                                    AND B.is_active = '1'" );

            return Datatables::of($data)
                    ->addIndexColumn()
                        ->addColumn('action', function($row) use ($idmenu){
                        $btn = '<a onclick="add_action(\'menu_role/update/'.$row->id_role.'/'.$idmenu.'\',\'_content_\',\'Master Client\');return false;" title="Atur Menu" class="btn btn-block btn-outline-success">Atur Menu</a>';
                            return $btn;
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

    }


	function update($id,$idmenu)
    {
		//$id = session('IdMsRole');
		$idmenu = $idmenu;
        $html = '';
        $x = 1;
        $cnt = 0;

		$MsRole = DB::table('MsRole')->where('IdMsRole','=',$id)->first();
		$NmRole = $MsRole->NmRole;
		$IsActive = $MsRole->IsActive;

		$select_status = $this->get_arr_combobox("SELECT * FROM TrReff WHERE Reff = 'Aktif' and IsActive = '1' ORDER BY Code asc", "Code", "IndUr", TRUE);
        $status = $this->form_dropdown('IsActive', $select_status, $IsActive, 'id="IsActivedp" text="Tahun" class="select form-control select2"');

		$arrRoleMenu = DB::table('MsRoleMenu')->where('IdMsRole','=',$id)->get();
		//dd($arrRoleMenu);


        foreach ($arrRoleMenu as $value) {
            $NewArrMenuRole['menu_role']['menu_id' . $value->IdMsMenu] = $value->IdMsMenu;
            $NewArrMenuRole['menu_role']['Created' . $value->IdMsMenu] = $value->Created;
            $NewArrMenuRole['menu_role']['Updated' . $value->IdMsMenu] = $value->Updated;
            $NewArrMenuRole['menu_role']['Approve' . $value->IdMsMenu] = $value->Approve;
            $NewArrMenuRole['menu_role']['Download' . $value->IdMsMenu] = $value->Download;
        }//dd($NewArrMenuRole);


        $arrMenu = DB::table('MsMenu')->select('IdMsMenu','KdIndukMenu as menu_main', 'NmMenu as menu_name','SeqMenu as menu_seq','Created','Updated','Approve','Download')->whereNotNull('KdIndukMenu')->where('IsActive','=','1')->orderBy('KdIndukMenu', 'asc')->orderBy('SeqMenu', 'asc')->get();


        foreach ($arrMenu as $val) {
            $menuid = $val->IdMsMenu;
            $Created = $val->Created;
            $Updated = $val->Updated;
            $Approve = $val->Approve;
            $Download = $val->Download;

			$data = DB::table('MsRoleMenu')->where('IdMsMenu', $menuid)->where('IdMsRole', $id)->first();

            if(!empty($NewArrMenuRole)){
              if (in_array($menuid,$NewArrMenuRole['menu_role'])) {
                  $rmkChked[$menuid] = 'checked';
                  $dsbl[$menuid] = '';
						if($NewArrMenuRole['menu_role']['Created' . $menuid] != NULL){
							$NewArrMenuRole['menu_role']['Created' . $menuid] = 'checked';
						}else{
							$NewArrMenuRole['menu_role']['Created' . $menuid] = '';
						}

						if($NewArrMenuRole['menu_role']['Updated' . $menuid]!= NULL){
							$NewArrMenuRole['menu_role']['Updated' . $menuid] = 'checked';
						}else{
							$NewArrMenuRole['menu_role']['Updated' . $menuid] = '';
						}

						if($NewArrMenuRole['menu_role']['Approve' . $menuid] != NULL){
							$NewArrMenuRole['menu_role']['Approve' . $menuid] = 'checked';
						}else{
							$NewArrMenuRole['menu_role']['Approve' . $menuid] = '';
						}

						if($NewArrMenuRole['menu_role']['Download' . $menuid] != NULL){
							$NewArrMenuRole['menu_role']['Download' . $menuid] = 'checked';
						}else{
							$NewArrMenuRole['menu_role']['Download' . $menuid] = '';
						}
				} else {
                  $rmkChked[$menuid] = '';
				  $NewArrMenuRole['menu_role']['Approve' . $menuid] = '';
				  $NewArrMenuRole['menu_role']['Created' . $menuid] = '';
				  $NewArrMenuRole['menu_role']['Updated' . $menuid] = '';
				  $NewArrMenuRole['menu_role']['Download' . $menuid] = '';
                  $dsbl[$menuid] = 'disabled';//disabled
              }
            }else{
              $rmkChked[$menuid] = '';
			  $NewArrMenuRole['menu_role']['Approve' . $menuid] = '';
			  $NewArrMenuRole['menu_role']['Created' . $menuid] = '';
			  $NewArrMenuRole['menu_role']['Updated' . $menuid] = '';
			  $NewArrMenuRole['menu_role']['Download' . $menuid] = '';
              $dsbl[$menuid] = 'disabled';
            }



              if ($val->menu_seq < '2') {
                  //print_r($arrMenu);die();
				  $html .= '<div class="col-md-4">';
                  $html .= '  <div class="kt-checkbox-inline"><label class="kt-checkbox">
                                  <input ' . $rmkChked[$val->IdMsMenu] . ' id="IdMsMenu' . $cnt . '" name="input[' . $cnt . '][IdMsMenu]" onclick="rmvDsl(\'' . $cnt . '\')" type="checkbox" value="' . $val->IdMsMenu . '">
                                  ' . $val->menu_name . '
                                  <span></span>
                              </label>
                              <input  ' . $dsbl[$val->IdMsMenu] . ' id="IdMsRole' . $cnt . '" name="input[' . $cnt . '][IdMsRole]" value="' . $id . '" type="hidden"/></div></div>';

					$html .= '<div class="col-md-8"><div class="kt-checkbox-inline">
							  </div></div>
                              ';
              } else {
                  $html .= '  <div class="col-md-4"><div class="kt-checkbox-inline"><label class="kt-checkbox" style="margin-left: 31px;">
                                  <input ' . $rmkChked[$val->IdMsMenu] . ' id="IdMsMenu' . $cnt . '" name="input[' . $cnt . '][IdMsMenu]" onclick="rmvDsl(\'' . $cnt . '\')" type="checkbox" value="' . $val->IdMsMenu . '">
                                  ' . $val->menu_name . '
                                  <span></span>
                              </label>
                              <input  ' . $dsbl[$val->IdMsMenu] . ' id="IdMsRole' . $cnt . '" name="input[' . $cnt . '][IdMsRole]" value="' . $id . '" type="hidden"/></div></div>
                              ';
				 $html .= '<div class="col-md-8"><div class="kt-checkbox-inline">';
				if($Created == 'C'){
				 $html .= '		<label class="kt-checkbox" >
									<input value="C" ' .  $NewArrMenuRole['menu_role']['Created' . $menuid] . '  id="Aksi' . $cnt . '" name="input[' . $cnt . '][Created]" type="checkbox"> Tambah
									<span></span>
								</label>';
				};
				if($Updated == 'U'){
				$html .= '		<label class="kt-checkbox">
									<input value="U"  ' .  $NewArrMenuRole['menu_role']['Updated' . $menuid] . '  id="Aksi' . $cnt . '" name="input[' . $cnt . '][Updated]" type="checkbox">  Ubah
									<span></span>
								</label>';
				};
				if($Approve == 'A'){
				$html .= '		<label class="kt-checkbox">
									<input value="A" ' .  $NewArrMenuRole['menu_role']['Approve' . $menuid] . '   id="Aksi' . $cnt . '" name="input[' . $cnt . '][Approve]" type="checkbox">  Approve
									<span></span>
								</label>';
				};
				if($Download == 'D'){
				$html .= '		<label class="kt-checkbox">
									<input value="D"  ' . $NewArrMenuRole['menu_role']['Download' . $menuid] . '  id="Aksi' . $cnt . '" name="input[' . $cnt . '][Download]" type="checkbox">  Download
									<span></span>
								</label>';
				};
				$html .= '</div></div>';
              }

              $cnt++;

        }



        return view('menu_role.update_menu_role', ['arrMenu'=>$arrMenu, 'status' => $status, 'html' => $html, 'role_id'=>$id,'idmenu'=>$idmenu, 'NmRole' => $NmRole ]);
    }

    function create($idmenu)
    {
		$idmenu = $idmenu;
        $id = session('IdMsRole');

        $html = '';
        $x = 1;
        $cnt = 0;


        $arrMenu = DB::table('MsMenu')->select('IdMsMenu','KdIndukMenu as menu_main', 'NmMenu as menu_name','SeqMenu as menu_seq','Created','Updated','Approve','Download')->whereNotNull('KdIndukMenu')->where('IsActive','=','1')->orderBy('KdIndukMenu', 'asc')->orderBy('SeqMenu', 'asc')->get();



        foreach ($arrMenu as $val) {
            $menuid = $val->IdMsMenu;
			$Created = $val->Created;
            $Updated = $val->Updated;
            $Approve = $val->Approve;
            $Download = $val->Download;

            if(!empty($NewArrMenuRole)){
              if (in_array($menuid,$NewArrMenuRole['menu_role'])) {
                  $rmkChked[$menuid] = 'checked';
                  $dsbl[$menuid] = '';
              } else {
                  $rmkChked[$menuid] = '';
                  $dsbl[$menuid] = 'disabled';//disabled
              }
            }else{
              $rmkChked[$menuid] = '';
              $dsbl[$menuid] = 'disabled';
            }


              if ($val->menu_seq < '2') {
                  //print_r($arrMenu);die();
				  $html .= '<div class="col-md-4">';
                  $html .= '  <div class="kt-checkbox-inline"><label class="kt-checkbox">
                                  <input ' . $rmkChked[$val->IdMsMenu] . ' id="IdMsMenu' . $cnt . '" name="input[' . $cnt . '][IdMsMenu]" onclick="rmvDsl(\'' . $cnt . '\')" type="checkbox" value="' . $val->IdMsMenu . '">
                                  ' . $val->menu_name . '
                                  <span></span>
                              </label>
                              </div></div>';

					 $html .= '<div class="col-md-8"><div class="kt-checkbox-inline">
							</div></div>
                              ';
              } else {
                  $html .= '  <div class="col-md-4"><div class="kt-checkbox-inline"><label class="kt-checkbox" style="margin-left: 31px;">
                                  <input ' . $rmkChked[$val->IdMsMenu] . ' id="IdMsMenu' . $cnt . '" name="input[' . $cnt . '][IdMsMenu]" onclick="rmvDsl(\'' . $cnt . '\')" type="checkbox" value="' . $val->IdMsMenu . '">
                                  ' . $val->menu_name . '
                                  <span></span>
                              </label>
                              </div></div>
                              ';
				 $html .= '<div class="col-md-8"><div class="kt-checkbox-inline">';
					if($Created == 'C'){
					$html .= '	<label class="kt-checkbox" >
									<input type="checkbox" value="C" id="Aksi' . $cnt . '" name="input[' . $cnt . '][Created]" type="checkbox"> Tambah
									<span></span>
								</label>';
					};
					if($Updated == 'U'){
					$html .= '	<label class="kt-checkbox">
									<input type="checkbox" value="U" id="Aksi' . $cnt . '" name="input[' . $cnt . '][Updated]" type="checkbox"> Ubah
									<span></span>
								</label>';
					};
					if($Approve == 'A'){
					$html .= '	<label class="kt-checkbox">
									<input type="checkbox" value="A" id="Aksi' . $cnt . '" name="input[' . $cnt . '][Approve]" type="checkbox"> Approve
									<span></span>
								</label>';
					};
					if($Download == 'D'){
					$html .= '	<label class="kt-checkbox">
									<input type="checkbox" value="D" id="Aksi' . $cnt . '" name="input[' . $cnt . '][Download]" type="checkbox"> Download
									<span></span>
								</label>';
					};
                    $html .= '</div></div>';
              }

              $cnt++;

        }


        return view('menu_role.create_menu_role', ['arrMenu'=>$arrMenu, 'html' => $html, 'role_id'=>$id,'idmenu'=>$idmenu]);
    }

    public function add_data(Request $request)
    {


		$arrMenu = $request->input;//dd($request);
        $NmRole = $request->NmRole;
        $idmenu = $request->idmenu;
        $IsActive = 1;


		$MsRole = array('NmRole' => $NmRole,'IsActive' => $IsActive);
		$id = DB::table('MsRole')->insertGetId($MsRole);
		DB::commit();

		foreach ($arrMenu as $k => $v) {
		  //if ($v['IdMsRole']==1) {
			$arrMenu[$k]['IdMsRole']= $id;
		 // }
		}//dd($arrMenu);


        if (is_array($arrMenu)) {
          foreach ($arrMenu as $value) {
              $exec = DB::table('MsRoleMenu')->insert($value);//$value; //$this->db->insert('tm_menu_role', $value);
          }

          DB::commit();
          $CkRole = DB::table('MsRoleMenu')->where('IdMsRole','=',$id)->get();
          if ($CkRole) {
              $hasil = 'MSG|OK|' . url('menu_role') . '|Insert Data Successed |'.$idmenu ;
          } else {
              $hasil = "MSG|ER|Data Gagal Disimpan";
          }
        }

        return $hasil;
    }

	 public function edit_data(Request $request)
    {
		$hasil = '';
        $arrMenu = $request->input;//dd($request);
        $role = $request->role_id;

		$NmRole = $request->NmRole;
		$IsActive = $request->IsActive;
		 $idmenu = $request->idmenu;


		$MsRole = array('NmRole' => $NmRole,'IsActive' => $IsActive);
		DB::table('MsRole')->where('IdMsRole', '=', $role)
                ->update($MsRole);

        $CkRole = DB::table('MsRoleMenu')->where('IdMsRole','=',$role)->get();
        if (is_array($arrMenu)) {
          if ($CkRole) {
              DB::table('MsRoleMenu')->where('IdMsRole',$request->role_id)->delete();
              DB::beginTransaction();
          }

          foreach ($arrMenu as $value) {
              $exec = DB::table('MsRoleMenu')->insert($value);//$value; //$this->db->insert('tm_menu_role', $value);
          }

          DB::commit();
          $CkRole = DB::table('MsRoleMenu')->where('IdMsRole','=',$role)->get();
          if ($CkRole) {
              $hasil = 'MSG|OK|' . url('menu_role') . '|Insert Data Successed |'.$idmenu ;
          } else {
              $msg = "MSG|ER|Data Gagal Disimpan";
          }
        }

        return $hasil;
    }


	public function export_excel(Request $request) {//dd($request);
		$request = $request->all();
        $date = date('Y-m-d');
        $fileName = 'Export Report.xls';
		return Excel::download(new ExportExcel($request), $fileName);
    }
}
