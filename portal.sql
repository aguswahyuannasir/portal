/*
 Navicat Premium Data Transfer

 Source Server         : mysql 8
 Source Server Type    : MySQL
 Source Server Version : 100417
 Source Host           : localhost:3306
 Source Schema         : portal

 Target Server Type    : MySQL
 Target Server Version : 100417
 File Encoding         : 65001

 Date: 06/10/2021 06:40:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for materis
-- ----------------------------
DROP TABLE IF EXISTS `materis`;
CREATE TABLE `materis`  (
  `id_materi` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `field_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_5` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_materi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of materis
-- ----------------------------
INSERT INTO `materis` VALUES (1, '1', '1', '1', '1', '1', NULL, NULL);
INSERT INTO `materis` VALUES (2, '2', '2', '2', '2', '2', NULL, NULL);
INSERT INTO `materis` VALUES (3, '3', '3', '3', '3', '3', NULL, NULL);
INSERT INTO `materis` VALUES (4, '4', '4', '4', '4', '4', NULL, NULL);

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id_menu` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `kode_menu` int NULL DEFAULT NULL,
  `nama_menu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `kode_induk_menu` int NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `order_menu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `seq_menu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `class_menu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `updated` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `download` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `approve` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `is_active` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 1, 'Home', 1, '#', '1', '1', 'fa fa-list', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (2, 2, 'Dashboard', 1, 'home', '1', '2', 'fa fa-list', NULL, 'C', 'C', NULL, NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (3, 3, 'List', 3, '#', '1', '1', 'fa fa-list', NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (4, 4, 'Transaksi', 3, 'transaksi', '1', '2', 'fa fa-list', NULL, NULL, NULL, 'D', NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (5, 5, 'Product', 3, 'product', '1', '2', 'fa fa-list', NULL, NULL, NULL, 'D', NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (6, 6, 'Materi', 3, 'materi', '1', '2', 'fa fa-list', NULL, NULL, NULL, 'D', NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (11, 11, 'Setup', 11, '#', '1', '1', 'fa fa-list', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL);
INSERT INTO `menus` VALUES (12, 12, 'Manajemen User', 11, 'user', '1', '2', 'fa fa-list', NULL, 'C', 'U', NULL, NULL, '1', NULL, NULL);
INSERT INTO `menus` VALUES (13, 13, 'Manajemen Role', 11, 'menu_role', '1', '2', 'fa fa-list', NULL, 'C', 'U', NULL, NULL, '1', NULL, NULL);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (3, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (5, '2021_10_04_105637_create_roles_table', 2);
INSERT INTO `migrations` VALUES (7, '2021_10_04_105904_create_menus_table', 2);
INSERT INTO `migrations` VALUES (8, '2021_10_04_105846_create_role_menus_table', 3);
INSERT INTO `migrations` VALUES (9, '2021_10_04_152903_create_reffs_table', 4);
INSERT INTO `migrations` VALUES (10, '2021_10_04_203128_create_transaksis_table', 5);
INSERT INTO `migrations` VALUES (11, '2021_10_04_203200_create_products_table', 5);
INSERT INTO `migrations` VALUES (12, '2021_10_04_203220_create_materis_table', 5);

-- ----------------------------
-- Table structure for products
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products`  (
  `id_product` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `field_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_5` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_product`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES (1, '1', '1', '1', '1', '1', NULL, NULL);
INSERT INTO `products` VALUES (2, '2', '2', '2', '2', '2', NULL, NULL);
INSERT INTO `products` VALUES (3, '3', '3', '3', '3', '3', NULL, NULL);

-- ----------------------------
-- Table structure for reffs
-- ----------------------------
DROP TABLE IF EXISTS `reffs`;
CREATE TABLE `reffs`  (
  `id_reff` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `code` int NULL DEFAULT NULL,
  `ind` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `eng` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `is_active` int NULL DEFAULT NULL,
  `reff` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id_reff`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of reffs
-- ----------------------------
INSERT INTO `reffs` VALUES (1, 1, 'Aktif', 'Aktive', 1, 'Aktif');
INSERT INTO `reffs` VALUES (2, 0, 'Tidak Aktif', 'Not Aktive', 1, 'Aktif');
INSERT INTO `reffs` VALUES (3, 0, 'Tidak', 'No', 1, 'YesNo');
INSERT INTO `reffs` VALUES (4, 1, 'Ya', 'Yes', 1, 'YesNo');
INSERT INTO `reffs` VALUES (5, 1, 'Kunci', 'Lock', 1, 'Lock');
INSERT INTO `reffs` VALUES (6, 0, 'Tidak Kunci', 'Not Lock', 1, 'Lock');

-- ----------------------------
-- Table structure for role_menus
-- ----------------------------
DROP TABLE IF EXISTS `role_menus`;
CREATE TABLE `role_menus`  (
  `id_role_menu` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_role` int NULL DEFAULT NULL,
  `id_menu` int NULL DEFAULT NULL,
  `created` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `updated` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `download` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `approve` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_role_menu`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 37 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_menus
-- ----------------------------
INSERT INTO `role_menus` VALUES (4, 2, 4, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (5, 2, 5, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (6, 2, 6, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (7, 2, 7, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (8, 3, 4, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (9, 3, 5, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (10, 3, 6, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (11, 3, 7, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (12, 4, 4, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (13, 4, 5, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (14, 4, 6, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (15, 4, 7, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (16, 5, 4, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (17, 5, 5, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (18, 5, 6, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (19, 5, 7, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (20, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (21, 1, 2, 'C', 'U', NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (22, 1, 3, 'C', 'U', NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (23, 1, 4, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (24, 1, 5, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (25, 1, 6, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (26, 1, 7, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (27, 1, 8, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (28, 1, 9, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (29, 1, 10, NULL, NULL, 'D', NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (30, 1, 11, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (31, 1, 12, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (32, 1, 13, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (35, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `role_menus` VALUES (36, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id_role` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_role` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `is_active` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (1, 'Admin', '1', NULL, NULL);
INSERT INTO `roles` VALUES (2, 'role1', '1', NULL, NULL);
INSERT INTO `roles` VALUES (3, 'role2', '1', NULL, NULL);
INSERT INTO `roles` VALUES (4, 'role3', '1', NULL, NULL);
INSERT INTO `roles` VALUES (5, 'role4', '1', NULL, NULL);

-- ----------------------------
-- Table structure for summarydashboard
-- ----------------------------
DROP TABLE IF EXISTS `summarydashboard`;
CREATE TABLE `summarydashboard`  (
  `TglPopulasi` datetime(0) NULL DEFAULT NULL,
  `Periode` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `TglData` date NULL DEFAULT NULL,
  `KdKPR` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NmKPR` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `KdKPA` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `NmKPA` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `AmtPTPSin` decimal(22, 4) NULL DEFAULT NULL,
  `AmtPTPReg` decimal(22, 4) NULL DEFAULT NULL,
  `Polis` int NULL DEFAULT NULL,
  `AgenMenutup` int NULL DEFAULT NULL,
  `AgenBaru` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of summarydashboard
-- ----------------------------
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '303', 'UPA Binjai Imam Bonjol', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '304', 'UPA LANGSA SUDIRMAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '305', 'UPA NIAS DIPONEGORO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '306', 'UPA Bireuen Medan Banda Aceh', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '307', 'UPA ACEH BARAT MEULABOH', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '309', 'UPA LHOKSEUMAWE MEDAN BANDA ACEH', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '310', 'UPA TAKENGON YOS SUDARSO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '315', 'UPA KABANJAHE SAMURA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '316', 'UPA SIDEMPUAN SUDIRMAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '318', 'UPA BALIGE P SIANTAR', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '324', 'UPA Padang Panjang Soekarno Hatta', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '327', 'UPA DURI HANG TUAH', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '331', 'UPA TANJUNG BALAI KARIMUN PERTAMBANGAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '334', 'UPA PRABUMULIH SUDIRMAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '335', 'UPA MUARA ENIM SULTAN MAHMUD BADARUDDIN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '336', 'UPA LAHAT MARTADINATA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '338', 'UPA Bangka Belinyu', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '339', 'UPA BELITUNG TANJUNG PANDAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '341', 'UPA BANGKO SUDIRMAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '344', 'UPA  KERINCI SITINJAU LAUT ', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '346', 'UPA Lubuk Linggau Timur I', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '347', 'KPA METRO AH NASUTION', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '350', 'KPA MENGGALA TULANG BAWANG', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '108', 'KPA Tangerang Pahlawan', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '113', 'UPA Cilegon Cibeber', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '114', 'KPA Serang Yusuf Martadilaga', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '117', 'UPA SINTANG MUJAHIDIN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '118', 'UPA KETAPANG WOLTER MONGINSIDI', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '120', 'UPA Palangkaraya Temanggung', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '121', 'UPA Pangkalan Bun Ahmad Wongso', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '134', 'KPA Depok Sukamaju', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '137', 'KPA Jakarta Biak High Network', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '138', 'KPA Jakarta Rasuna Said', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '139', 'KANTOR PROTEKSI PRADANA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '124', 'UPA Soreang Cingcin', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '128', 'UPA Subang Otista', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '250', 'UPA PEMALANG GATOT SUBROTO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '251', 'KPA PURBALINGGA A YANI', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '258', 'UPA BLORA AGIL KUSUMADIYA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '262', 'KPA JEPARA KOL SUGIONO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '234', 'UPA Kebumen Pemuda', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '235', 'UPA PURWOREJO TENTARA PELAJAR', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '238', 'UPA KARANGANYAR LAWU KADIPIRO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '241', 'KPA SUKOHARJO JLOPO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '243', 'UPA Temanggung Dr. Cipto', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '244', 'UPA SALATIGA SIDOREJO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '202', 'UPA Pamekasan Lawangan Daya', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '210', 'UPA JOMBANG SOEKARNO HATTA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '211', 'KPA Nganjuk Gatot Subroto', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '216', 'UPA PACITAN AHMAD YANI', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '217', 'UPA Ponorogo Arief Rahman Hakim', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '218', 'UPA Madiun Manguharjo', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '223', 'UPA PASURUAN WAHIDIN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '225', 'KPA Lumajang Gatot Subroto', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '226', 'KPA Jember PB Sudirman', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '228', 'UPA BANYUWANGI AGUS SALIM', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '231', 'KPA Jember Kaliwates', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '232', 'UPA Batu Untung Suropati', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '421', 'UPA PASIR SINGA MAULANA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '422', 'UPA TANJUNG REDEB AKB SANIPAH', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '423', 'UPA TARAKAN SLAMET RIYADI', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '424', 'UPA BONTANG KS TUBUN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '425', 'KPA Segiri Pembangunan', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '426', 'UPA SANGATA APT PRANOTO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '427', 'UPA TENGGARONG PESUT', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '428', 'KPA Banjarmasin Lingkar Dalam Selatan', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '429', 'UPA BARABAI PHM NOOR', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '430', 'UPA KOTA BARU RAYA BATULICIN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '402', 'UPA NEGARA RATNA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '410', 'UPA LOMBOK TIMUR SELONG', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '411', 'UPA SUMBAWA GARUDA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '412', 'UPA ATAMBUA ADI SUCIPTO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '413', 'UPA BAJAWA YOS SIDARSO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '414', 'UPA ENDE RAMBUTAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '416', 'UPA LARANTUKA DON LORENSO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '417', 'UPA MAUMERE CEMPAKA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '419', 'UPA SUMBA TIMUR ELTARY', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '431', 'UPA PANGKEP MATAHARI', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '432', 'UPA PARE PARE KARAENG BURA`NE', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '437', 'UPA WATAMPONE ACHMAD YANI', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '438', 'UPA PALOPO SUDIRMAN', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '439', 'UPA MASAMBA MOH HATTA', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '441', 'UPA MAMUJU ANGGREK', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '444', 'UPA Kolaka Wundulako', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '445', 'UPA Raha Sultan Hasanuddin', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '448', 'UPA KOTAMOBAGU PONTODON', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '449', 'UPA Ternate Tengah Maliaro', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '451', 'UPA GORONTALO BUDI UTOMO', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '454', 'UPA Timika Nawaripi', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '456', 'UPA WAMENA HOM HOM', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '457', 'UPA Manokwari Trikora Wosi', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '459', 'UPA Tual Leimena', 0.0000, 0.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '301', 'KPA Medan Lubuk Pakam', 60000000.0000, 10788774.0000, 4, 3, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '302', 'KPA Medan Diponegoro', 684000000.0000, 19266755.0000, 13, 7, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '308', 'KPA Banda Aceh Chik Ditiro', 1000000000.0000, 4748120.0000, 5, 1, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '311', 'KPA Kisaran SM Raja', 0.0000, 4443067.0000, 5, 1, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '312', 'KPA Tanjung Balai Teuku Umar', 0.0000, 28912454.0000, 7, 2, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '313', 'KPA Rantau Prapat SM Raja', 690000000.0000, 14234045.0000, 13, 10, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '314', 'KPA Pematang Siantar Sutomo', 0.0000, 506450.0000, 0, 0, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R01', 'MEDAN', '317', 'UPA Sibolga Horas', 0.0000, 3450000.0000, 0, 0, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '319', 'KPA Payakumbuh Tan Malaka', 800000000.0000, 20380670.0000, 21, 7, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '320', 'UPA Solok Kubung', 10000000.0000, 250000.0000, 10, 10, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '321', 'KPA Padang Sudirman', 552000000.0000, 38463615.0000, 43, 22, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '322', 'KPA Pariaman Imam Bonjol', 0.0000, 3150000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '323', 'KPA Bukit Tinggi Gulai Bancah', 195224057.0000, 330640.0000, 2, 2, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '325', 'UPA Bangkinang Yamin', 5000000.0000, 2444000.0000, 6, 6, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '326', 'KPA Dumai Sukajadi', 607000000.0000, 11755281.0000, 21, 13, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '328', 'KPA Rengat Padang Reba', 0.0000, 2300000.0000, 0, 0, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '329', 'KPA Pekanbaru Sudirman', 300000000.0000, 7247480.0000, 6, 2, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '330', 'KPA Batam Batu Aji', 0.0000, 3510380.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '332', 'UPA Tanjung Pinang Nila', 0.0000, 1680770.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R02', 'PADANG', '352', 'UPA Painan Sudirman', 0.0000, 2663410.0000, 5, 1, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '333', 'KPA Baturaja Moh Hatta', 0.0000, 1500000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '337', 'KPA Palembang Sudirman', 163000000.0000, 10335740.0000, 36, 27, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '340', 'KPA Jambi S Brojonegoro', 803000000.0000, 1631500.0000, 7, 7, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '342', 'KPA Kuala Tungkal Sudirman', 100000000.0000, 2450000.0000, 1, 1, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '343', 'KPA Muara Bungo Sudirman', 0.0000, 250000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '345', 'KPA Bengkulu Raflesia', 0.0000, 1450000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '348', 'KPA Kalianda Kesuma Bangsa', 27000000.0000, 19950000.0000, 33, 19, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '349', 'KPA Lampung Kotabumi', 11000000.0000, 16784159.0000, 16, 6, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '351', 'KPA Bandar Lampung Wolter Monginsidi', 198000000.0000, 4753093.0000, 33, 18, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '353', 'UPA Natar Sukarame', 54000000.0000, 6860645.0000, 47, 24, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '354', 'UPA Karang Sari Senopati', 0.0000, 1051705.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R03', 'PALEMBANG', '355', 'UPA Sukarame Raden Intan', 0.0000, 900000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '103', 'KPA Jakarta Simatupang', 1738000000.0000, 15352111.0000, 31, 25, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '104', 'KPA Jakarta Kalimalang', 595000000.0000, 6117210.0000, 56, 42, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '105', 'KPA Cikampek A Yani', 12000000.0000, 2840900.0000, 12, 11, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '106', 'KPA Jakarta Biak', 11876500000.0000, 56464120.0000, 85, 64, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '110', 'KPA Tangerang KS Tubun', 187000000.0000, 23191123.0000, 13, 10, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '112', 'KPA Tangerang Raya Serang', 9000000.0000, 10853842.0000, 10, 10, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '115', 'KPA Pontianak A Yani', 0.0000, 5959850.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '119', 'UPA Muara Teweh Yetro Sinseng', 0.0000, 484820.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '129', 'KPA Depok Cinere', 2105000000.0000, 7185700.0000, 10, 8, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '130', 'KPA Depok Margonda', 65000000.0000, 23365667.0000, 31, 26, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R04', 'JAKARTA', '133', 'KPA Bogor Siliwangi', 105000000.0000, 8501665.0000, 7, 5, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '122', 'KPA Bandung Cikawao', 1566141859.0000, 5868880.0000, 18, 9, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '123', 'KPA Cimahi Kotamas', 63000000.0000, 23626785.0000, 19, 17, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '126', 'KPA Ciamis Komplek Imbanagara', 10000000.0000, 4430400.0000, 10, 10, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '127', 'KPA Cirebon Tuparev', 128000000.0000, 3051390.0000, 45, 25, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '131', 'KPA Sukabumi Brawijaya', 375000000.0000, 2090820.0000, 24, 24, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '132', 'KPA Cianjur Otista', 1000000.0000, 2314920.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R05', 'BANDUNG', '135', 'UPA Indramayu Adipati Karna', 0.0000, 1800800.0000, 2, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '248', 'KPA Cilacap Gatot Subroto', 1250000000.0000, 2000612.0000, 3, 2, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '249', 'KPA Tegal Kapten Sudibyo', 100000000.0000, 9774324.0000, 6, 2, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '252', 'KPA Purwokerto RA Wiryaatmaja', 62000000.0000, 10155723.0000, 19, 12, 4);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '253', 'KPA Pekalongan Sudirman', 40000000.0000, 1847325.0000, 2, 2, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '254', 'UPA Batang Ki Mangunsarkoro', 2000000.0000, 700000.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '255', 'UPA Kendal Pelita', 383000000.0000, 650000.0000, 8, 8, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '256', 'KPA Ungaran Diponegoro', 193000000.0000, 9055520.0000, 26, 19, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '257', 'KPA Semarang Gajahmada', 159412000.0000, 12950329.0000, 21, 9, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '259', 'KPA Puwodadi Gajah Mada', 54000000.0000, 6454018.0000, 12, 7, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '260', 'KPA Kudus Pramuka', 101000000.0000, 7480813.0000, 8, 3, 6);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R06', 'SEMARANG', '261', 'UPA Pati Mulyoharjo', 60000000.0000, 17404477.0000, 7, 3, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '233', 'UPA Bantul Trirenggo', 591000000.0000, 550450.0000, 4, 3, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '236', 'KPA Yogyakarta Wates', 705254580.0000, 37583723.0000, 99, 76, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '237', 'KPA Yogyakarta Adisucipto', 2203000000.0000, 25203771.0000, 120, 97, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '239', 'UPA Wonosari Veteran', 53000000.0000, 403005.0000, 4, 3, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '240', 'KPA Sragen Sukowati', 273000000.0000, 14342037.0000, 32, 17, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '242', 'KPA Surakarta Slamet Riyadi', 1000000.0000, 4971109.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '245', 'KPA Boyolali Kates', 5000000.0000, 3096470.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '246', 'KPA Klaten Mayor Sunaryo', 119000000.0000, 19958620.0000, 24, 19, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R07', 'YOGYAKARTA', '247', 'KPA Magelang Sarwo Edi Wibowo', 105000000.0000, 13992319.0000, 10, 10, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '201', 'KPA Surabaya Kartini', 774000000.0000, 32299955.0000, 72, 56, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '203', 'KPA Sidoarjo Kletek', 162000000.0000, 14138740.0000, 27, 20, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '204', 'KPA Gresik Green Garden', 53000000.0000, 3012300.0000, 5, 5, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '205', 'UPA Lamongan Demangan Regency', 0.0000, 2400000.0000, 5, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '206', 'KPA Mojokerto Pahlawan', 353000000.0000, 20701000.0000, 28, 17, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '207', 'UPA Tuban Agus Salim', 1250000000.0000, 1779100.0000, 5, 3, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '208', 'UPA Blitar Soedanco Supriyadi', 0.0000, 12500000.0000, 2, 2, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '209', 'KPA Kediri Hasanudin', 432000000.0000, 21379100.0000, 50, 46, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '212', 'UPA Tulungagung Ngunut', 109000000.0000, 0.0000, 10, 10, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '213', 'KPA Tulungagung Diponegoro', 403000000.0000, 12571200.0000, 85, 68, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '214', 'UPA Blitar Talun', 8000000.0000, 300000.0000, 8, 8, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '215', 'KPA Trenggalek Ngantru', 276000000.0000, 9905650.0000, 23, 17, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '219', 'KPA Ngawi Ahmad Yani', 103000000.0000, 15125400.0000, 20, 17, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '220', 'KPA Magetan Bangka', 108000000.0000, 9670770.0000, 12, 9, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '221', 'KPA Malang A Yani', 400000000.0000, 77937726.0000, 82, 68, 3);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '222', 'KPA Pasuruan Pandaan', 102000000.0000, 14649662.0000, 39, 31, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '224', 'KPA Probolinggo Panjaitan', 255000000.0000, 7077854.0000, 6, 5, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '227', 'KPA Malang Kahuripan', 290000000.0000, 15879984.0000, 76, 59, 4);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '229', 'UPA Bondowoso Diponegoro', 0.0000, 950000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '230', 'UPA Banyuwangi Genteng', 0.0000, 420160.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '263', 'UPA Bojonegoro Lisman', 0.0000, 2615700.0000, 5, 1, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R08', 'SURABAYA', '420', 'KPA Balikpapan A Yani', 339000000.0000, 11477100.0000, 11, 9, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '401', 'KPA Singaraja Sutomo', 0.0000, 248900.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '403', 'KPA Tabanan Tukad Balian', 514000000.0000, 41058300.0000, 32, 19, 5);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '404', 'KPA Denpasar Gatot Subroto', 152000000.0000, 11574780.0000, 9, 8, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '405', 'KPA Gianyar Dharma Giri', 207000000.0000, 28049119.0000, 44, 30, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '406', 'UPA Bima Gatot Subroto', 0.0000, 700000.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '407', 'KPA Dompu Karijawa', 95000000.0000, 3427425.0000, 7, 6, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '408', 'UPA Praya Perkutut', 0.0000, 205800.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '409', 'KPA Mataram Rembiga', 100000000.0000, 2447750.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '415', 'KPA Kupang Ahmad Yani', 0.0000, 252000.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '418', 'UPA Manggarai Satar Tacik', 0.0000, 19051600.0000, 6, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R09', 'DENPASAR', '462', 'KPM Denpasar Suli', 202000000.0000, 130939062.0000, 11, 6, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '434', 'KPA Makassar Andi Pangeran Pettarani', 51000000.0000, 5741900.0000, 3, 3, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '435', 'UPA Sengkang Hasanuddin', 40000000.0000, 741200.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '436', 'UPA Bulukumba Dato Tiro', 0.0000, 4795800.0000, 1, 1, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '440', 'UPA Toraja Utara Rantepao', 0.0000, 732400.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '442', 'UPA Bau Bau Kokalukuna', 0.0000, 9706330.0000, 5, 1, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '443', 'KPA Kendari Sorumba', 0.0000, 2409500.0000, 0, 0, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '446', 'UPA Konawe Unaha', 0.0000, 1836100.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '447', 'KPA Manado Piere Tendean', 0.0000, 2043300.0000, 0, 0, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '450', 'KPA Palu Yos Sudarso', 0.0000, 7414700.0000, 4, 3, 1);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '452', 'KPA Jayapura Ardipura', 2700000000.0000, 12504740.0000, 6, 6, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '453', 'KPA Merauke Mandala', 0.0000, 53297800.0000, 3, 3, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '455', 'KPA Biak Condronegoro', 0.0000, 3697704.0000, 1, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '458', 'UPA Ambon Laksadaya Wattimena', 0.0000, 7962600.0000, 6, 2, 2);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '460', 'UPA Nabire Sawo', 0.0000, 7021214.0000, 2, 1, 0);
INSERT INTO `summarydashboard` VALUES ('2021-03-30 09:25:48', '202101', '2021-01-31', 'R10', 'MAKASSAR', '461', 'UPA Sorong Puncak Cendrawasih', 0.0000, 300000.0000, 0, 0, 0);

-- ----------------------------
-- Table structure for transaksis
-- ----------------------------
DROP TABLE IF EXISTS `transaksis`;
CREATE TABLE `transaksis`  (
  `id_transaksi` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `field_1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_4` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `field_5` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id_transaksi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksis
-- ----------------------------
INSERT INTO `transaksis` VALUES (1, '1', '1', '1', '1', '1', NULL, NULL);
INSERT INTO `transaksis` VALUES (2, '2', '2', '2', '2', '2', NULL, NULL);
INSERT INTO `transaksis` VALUES (3, '3', '3', '3', '3', '3', NULL, NULL);
INSERT INTO `transaksis` VALUES (4, '4', '4', '4', '4', '4', NULL, NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id_user` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `kode_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `id_role` int NULL DEFAULT NULL,
  `is_active` int NULL DEFAULT NULL,
  `is_lock` int NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `attemps` int NULL DEFAULT NULL,
  PRIMARY KEY (`id_user`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Jhon', '', 'admin', NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
