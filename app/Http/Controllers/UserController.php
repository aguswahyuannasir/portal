<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;
Use DataTables;
use App\Http\Traits\ActMain;
use App\Http\Traits\Helper;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
	use ActMain;
    use Helper;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    function index(Request $request)
    {

		$input = $request->all();
		$id_menu = $request->input('idmenu');
		$id_role = session('id_role');
		$kode_user = session('kode_user');

		$role = DB::table('role_menus')->select('*')->where('id_menu', '=', $id_menu)->where('id_role', '=', $id_role)->first();

        $Created = $role->created;
		if($Created != ""){
			$Created = $role->created;
		}else{
			$Created = 'none';
		}

		return view('user.index',['Created'=>$Created,'id_menu'=>$id_menu]);
    }


    function data(Request $request)
    {

        if(request()->ajax())
        {
            $idmenu = $request->idmenu;

            $data = DB::select("SELECT
                                    A.id_user,
                                    A.kode_user,
                                    A.name,
                                    B.ind AS is_active_ur,
                                    C.Ind AS is_lock_ur,
                                    D.nama_role,
                                    A.email,
                                    A.is_lock
                                FROM
                                    users A
                                    INNER JOIN reffs B ON B.reff = 'Aktif' AND B.code = A.is_active AND B.is_active = '1'
                                    INNER JOIN reffs C ON C.reff = 'Lock' AND C.CODE = A.is_lock AND C.is_active = '1'
                                    LEFT JOIN roles D ON D.id_role = A.id_role
                                " );
        return Datatables::of($data)
                    ->addIndexColumn()
                        ->addColumn('action', function($row) use ($idmenu){
                        $btn = '<a onclick="add_action(\'user/update/'.$row->id_user.'/'.$idmenu.'\',\'_content_\',\'Master Client\');return false;" title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-md"><i class="la la-edit"></i></a>';
                            return $btn;
                        })
                        ->editColumn('is_lock', function ($data) {
                            if ($data->IsLock == 0) return 'T';
                            return 'Y';
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        }

    }




    public function delete($id)
    {

      DB::table('TX_USERS')->where('user_id',$id)->delete();

      $hasil = 'MSG|OK|' . url('user') . '|Delete Data User Berhasil';

      return $hasil;
    }

	function create($idmenu ) {
		$idmenu = $idmenu;
		$IdMsRole = session('IdMsRole');

		$Querydomain = DB::table('SystemParameter')
					->where('KdParameter', '=', 'DOMAIN')
					->where('IsActive', '=', 1)
					->first();

		$domain = $Querydomain->Value;

		$header = '(NEW)';

		$select_userrole = $this->get_arr_combobox("SELECT * FROM MsRole Where IsActive = '1' ORDER BY IdMsRole asc", "IdMsRole", "NmRole", TRUE);
		$user_role = $this->form_dropdown_user('user[IdMsRole]', $select_userrole, "", 'id="IdMsRole" text="role" class="select form-control select2"');

		$select_status = $this->get_arr_combobox("SELECT * FROM TrReff WHERE Reff = 'Aktif' and IsActive = '1'  ORDER BY Code asc", "Code", "IndUr", TRUE);
        $status = $this->form_dropdown_user('user[IsActive]', $select_status, "", 'id="IsActive" text="Tahun" class="select form-control select2"');


		$select_ldap = $this->get_arr_combobox("SELECT * FROM TrReff WHERE Reff = 'YesNo' and IsActive = '1'  ORDER BY Code asc", "Code", "IndUr", TRUE);
        $ldap = $this->form_dropdown_user('user[IsLDAP]', $select_ldap, "1", 'id="IsLDAP" disabled text="Tahun" class="select form-control select2"');

        return view('user.create', ['status' => $status,'userrole' => $user_role,'ldap' => $ldap, 'domain' => $domain, 'header' => $header,'idmenu'=>$idmenu]);
    }

	function update($id,$idmenu) {
		$idmenu = $idmenu;

		$header = '(EDIT)';

        $view_user = DB::table('MsUser')->where('IdMsUser', $id)->first();
        $IdMsRole = $view_user->IdMsRole;
        $IsLDAP = $view_user->IsLDAP;
        $IsActive = $view_user->IsActive;
        $IdMsRole = $view_user->IdMsRole;

        $select_userrole = $this->get_arr_combobox("SELECT * FROM MsRole Where IsActive = '1' ORDER BY IdMsRole asc", "IdMsRole", "NmRole", TRUE);
		$user_role = $this->form_dropdown_user('user[IdMsRole]', $select_userrole, $IdMsRole, 'id="IdMsRole" text="role" class="select form-control select2"');

		$select_status = $this->get_arr_combobox("SELECT * FROM TrReff WHERE Reff = 'Aktif' and IsActive = '1'  ORDER BY Code asc", "Code", "IndUr", TRUE);
        $status = $this->form_dropdown_user('user[IsActive]', $select_status, $IsActive, 'id="IsActive" text="Tahun" class="select form-control select2"');


		$select_ldap = $this->get_arr_combobox("SELECT * FROM TrReff WHERE Reff = 'YesNo' and IsActive = '1'  ORDER BY Code asc", "Code", "IndUr", TRUE);
        $ldap = $this->form_dropdown_user('user[IsLDAP]', $select_ldap,  $IsLDAP, 'id="IsLDAP" disabled text="Tahun" class="select form-control select2"');


        return view('user.update', ['user' => $view_user, 'status' => $status,'userrole' => $user_role,'ldap' => $ldap, 'header' => $header,'idmenu'=>$idmenu]);
    }

	 public function edit_data(Request $request) {

		$user = $request->user;
		 $idmenu = $request->idmenu;
		$user['TglUpdate'] = NOW();
		$user['IsLDAP'] = 1;
		$user['UserUpdate'] = session('NmUser');

        $users = DB::table('MsUser')->where('IdMsUser', '=', $request->input('IdMsUser'))
                ->update($user);


        $hasil = 'MSG|OK|' . url('user') . '|Update Data Successed |'.$idmenu ;

        return $hasil;
    }

	public function add_data(Request $request) {

		$user = $request->user;
		$idmenu = $request->idmenu;

		$Querydomain = DB::table('SystemParameter')
					->where('KdParameter', '=', 'DOMAIN')
					->where('IsActive', '=', 1)
					->first();

		$domain = $Querydomain->Value;
		$email = $user['Email'].$domain;

        //random password
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        $random_pass = $randomString;
		$user['Password'] = Hash::make($random_pass);
		$user['IsLDAP'] = 1;
		$user['IsLock'] = 0;
		$user['IsActive'] = 1;
		$user['TglCreate'] = NOW();
		$user['UserCreate'] = session('NmUser');
		$user['Email'] = $email;

        //save to db
        $users = DB::table('MsUser')
                ->insert($user);


        $hasil = 'MSG|OK|' . url('user') . '|Insert Data Successed |'.$idmenu ;


        return $hasil;
    }



	public function ceKdUser(Request $request) {

		$KdUser = $request->val;

		try {
			$results = DB::table('MsUser')->where('KdUser', $KdUser)->first();
			if($results == NULL){
				$hasil = 'OK';
			}else{
				$hasil = 'NO';
			}
		}catch (\Exception $e) {
			$hasil = 'NO';
		}

        return $hasil;
    }

	public function cekEmail(Request $request) {

		$Email = $request->val;

		try {
			$results = DB::table('MsUser')->where('Email', $Email)->first();
			if($results == NULL){
				$hasil = 'OK';
			}else{
				$hasil = 'NO';
			}
		}catch (\Exception $e) {
			$hasil = 'NO';
		}

        return $hasil;
    }
}
