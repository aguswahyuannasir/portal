var csrf = $('meta[name="_token"]').attr('content');

$(".LoginUser").submit(function (e) {
    e.preventDefault();
    loading();
	var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var form = $(this);
    var url = form.attr('action');

	if( username == ""){
        clearLoading()
        Swal.fire('Error', 'Silahkan Input Username atau Password', 'error');
        return false;
	}else if(password == ""){
		clearLoading()
		Swal.fire('Error', 'Silahkan Input Username atau Password', 'error');
		return false;
	}else{
		$.ajax({
			type: 'POST',
			url: url,
			data: form.serialize(),
			success: function (data) {
				var arrData = data.split('|');
				if (arrData[0].trim() === 'OK') {
					clearLoading();
					//Swal.fire('Success', arrData[1], 'success');
					setTimeout(function () {
						location.href = arrData[2];
					}, 1500);
				} else {
					clearLoading();
					Swal.fire('Error', arrData[1], 'error');
					return false;
				}
			}
		});
	}
});

function loading() {
    $('.page-loader').show();
}

function clearLoading() {
    $('.page-loader').delay(1000).fadeOut("slow");
}
