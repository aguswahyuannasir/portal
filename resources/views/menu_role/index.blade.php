<div class="kt-portlet kt-portlet--mobile" style="margin-top: 50px">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
				<i class="kt-font-brand flaticon2-line-chart"></i>
			</span>
            <h3 class="kt-portlet__head-title">
				 Manajemen Role
			</h3>
        </div>
         <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
				@if($Created =='C')
                    <button onclick="add_action('menu_role/create/{{$id_menu}}','_content_','Master Client');return false;" class="btn btn-brand btn-elevate btn-icon-sm"><i class="la la-plus"></i>Tambah Role</button>
				@endif
                </div>
            </div>
        </div>
    </div>

    <div class="kt-portlet__body">
        <div class="table-responsive" style="margin-top: 15px">
			<table class="table table-bordered table-striped" id="order_table">
				<thead class="thead">
					<tr>
						<th>No.</th>
						<th>ID Role</th>
						<th>Nama Role</th>
						<th>Status</th>
						<th>Aksi</th>
					</tr>
				</thead>
			</table>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function() {
            $('.input-daterange').datepicker({
                todayBtn: 'linked',
                format: 'yyyy-mm-dd',
                autoclose: true
            });


            load_data();

            function load_data(from_date = '', to_date = '') {
                $('#order_table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: {
                        url: 'menu_role/data',
                        data: {
                            idmenu: {{$id_menu}}
                        }
                    },
                    columns: [{data: 'DT_RowIndex', orderable: false, searchable: false
						},{
                            data: 'id_role',
                            name: 'id_role'
                        }, {
                            data: 'nama_role',
                            name: 'nama_role'
                        }, {
                            data: 'is_active',
                            name: 'is_active'
                        },
                        {data: 'action', name: 'action', orderable: false, searchable: false}
                    ]
                });
            }

        });


    </script>
