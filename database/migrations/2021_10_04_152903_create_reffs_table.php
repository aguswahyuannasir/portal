<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reffs', function (Blueprint $table) {
            $table->bigIncrements('id_reff');
            $table->integer('code')->nullable();
            $table->string('ind')->nullable();
            $table->string('eng')->nullable();
            $table->integer('is_active')->nullable();
            $table->string('reff')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reffs');
    }
}
