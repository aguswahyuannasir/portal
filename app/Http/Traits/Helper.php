<?php
namespace App\Http\Traits;
use DB;


trait Helper {
	public function form_dropdown_user($data = '', $options = array(), $selected = array(), $extra = '')
    {
        $defaults = array();

        if (is_array($data))
        {
            if (isset($data['selected']))
            {
                $selected = $data['selected'];
                unset($data['selected']); // select tags don't have a selected attribute
            }

            if (isset($data['options']))
            {
                $options = $data['options'];
                unset($data['options']); // select tags don't use an options attribute
            }
        }
        else
        {
            $defaults = array('name' => $data);
        }

        is_array($selected) OR $selected = array($selected);
        is_array($options) OR $options = array($options);


        // If no selected state was submitted we will attempt to set it automatically
        if (empty($selected))
        {
            if (is_array($data))
            {
                if (isset($data['name'], $_POST[$data['name']]))
                {
                    $selected = array($_POST[$data['name']]);
                }
            }
            elseif (isset($_POST[$data]))
            {
                $selected = array($_POST[$data]);
            }
        }

        $extra = $this->_attributes_to_string($extra);

        $multiple = (count($selected) > 1 && stripos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select '.rtrim($this->_parse_form_attributes($data, $defaults)).$extra.$multiple.">";

        foreach ($options as $key => $val)
        {
            $key = (string) $key;

            if (is_array($val))
            {
                if (empty($val))
                {
                    continue;
                }

                $form .= '<optgroup label="'.$key."\">";

                foreach ($val as $optgroup_key => $optgroup_val)
                {
                    $sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
                    $form .= '<option value="'.htmlspecialchars($optgroup_key).'"'.$sel.'>'
                        .(string) $optgroup_val."</option>";
                }

                $form .= "</optgroup>";
            }
            else
            {
                $form .= '<option value="'.htmlspecialchars($key).'"'
                    .(in_array($key, $selected) ? ' selected="selected"' : '').'>'
                    .(string) $val."</option>";
            }
        }
        return $form."</select>";
    }
	
	
  public function form_dropdown($data = '', $options = array(), $selected = array(), $extra = '')
    {
        $defaults = array();

        if (is_array($data))
        {
            if (isset($data['selected']))
            {
                $selected = $data['selected'];
                unset($data['selected']); // select tags don't have a selected attribute
            }

            if (isset($data['options']))
            {
                $options = $data['options'];
                unset($data['options']); // select tags don't use an options attribute
            }
        }
        else
        {
            $defaults = array('name' => $data);
        }

        is_array($selected) OR $selected = array($selected);
        is_array($options) OR $options = array($options);


        // If no selected state was submitted we will attempt to set it automatically
        if (empty($selected))
        {
            if (is_array($data))
            {
                if (isset($data['name'], $_POST[$data['name']]))
                {
                    $selected = array($_POST[$data['name']]);
                }
            }
            elseif (isset($_POST[$data]))
            {
                $selected = array($_POST[$data]);
            }
        }

        $extra = $this->_attributes_to_string($extra);

        $multiple = (count($selected) > 1 && stripos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';

        $form = '<select '.rtrim($this->_parse_form_attributes($data, $defaults)).$extra.$multiple.">";

        foreach ($options as $key => $val)
        {
            $key = (string) $key;

            if (is_array($val))
            {
                if (empty($val))
                {
                    continue;
                }

                $form .= '<optgroup label="'.$key."\">";

                foreach ($val as $optgroup_key => $optgroup_val)
                {
                    $sel = in_array($optgroup_key, $selected) ? ' selected="selected"' : '';
                    $form .= '<option value="'.htmlspecialchars($optgroup_key).'"'.$sel.'>'
                        .(string) $optgroup_val."</option>";
                }

                $form .= "</optgroup>";
            }
            else
            {
				$val = $val == '&nbsp;' ? '--semua--' : $val;
				$key = $val == '&nbsp;' ? '' : $key;
                $form .= '<option value="'.htmlspecialchars($key).'"'
                    .(in_array($key, $selected) ? ' selected="selected"' : '').'>'
                    .(string) $val."</option>";
            }
        }
        return $form."</select>";
    }

    function _parse_form_attributes($attributes, $default)
  {
    if (is_array($attributes))
    {
      foreach ($default as $key => $val)
      {
        if (isset($attributes[$key]))
        {
          $default[$key] = $attributes[$key];
          unset($attributes[$key]);
        }
      }

      if (count($attributes) > 0)
      {
        $default = array_merge($default, $attributes);
      }
    }

    $att = '';

    foreach ($default as $key => $val)
    {
      if ($key === 'value')
      {
        $val = html_escape($val);
      }
      elseif ($key === 'name' && ! strlen($default['name']))
      {
        continue;
      }

      $att .= $key.'="'.$val.'" ';
    }

    return $att;
  }


  function _attributes_to_string($attributes)
  {
    if (empty($attributes))
    {
      return '';
    }

    if (is_object($attributes))
    {
      $attributes = (array) $attributes;
    }

    if (is_array($attributes))
    {
      $atts = '';

      foreach ($attributes as $key => $val)
      {
        $atts .= ' '.$key.'="'.$val.'"';
      }

      return $atts;
    }

    if (is_string($attributes))
    {
      return ' '.$attributes;
    }

    return FALSE;
  }

  function penyebut($nilai) {
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
      $temp = " ". $huruf[$nilai];
    } else if ($nilai <20) {
      $temp = $this->penyebut($nilai - 10). " Belas";
    } else if ($nilai < 100) {
      $temp = $this->penyebut($nilai/10)." Puluh". $this->penyebut($nilai % 10);
    } else if ($nilai < 200) {
      $temp = " Seratus" . $this->penyebut($nilai - 100);
    } else if ($nilai < 1000) {
      $temp = $this->penyebut($nilai/100) . " Ratus" . $this->penyebut($nilai % 100);
    } else if ($nilai < 2000) {
      $temp = " Seribu" . $this->penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
      $temp = $this->penyebut($nilai/1000) . " Ribu" . $this->penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
      $temp = $this->penyebut($nilai/1000000) . " Juta" . $this->penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
      $temp = $this->penyebut($nilai/1000000000) . " Milyar" . $this->penyebut(fmod($nilai,1000000000));
    } else if ($nilai < 1000000000000000) {
      $temp = $this->penyebut($nilai/1000000000000) . " Trilyun" . $this->penyebut(fmod($nilai,1000000000000));
    }     
    return $temp;
  }
 
  function terbilang($nilai) {
    if($nilai<0) {
      $hasil = "minus ". trim($this->penyebut($nilai));
    } else {
      $hasil = trim($this->penyebut($nilai));
    }         
    return $hasil;
  }

  function bulan($angka){
    $kumpulanBulan="Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember";

    $arrayBulan=explode(" ", $kumpulanBulan);

    return $arrayBulan[$angka-1];
  }

  function hari($angka){
    $kumpulanHari="Senin Selasa Rabu Kamis Jumat Sabtu Minggu";

    $arrayHari=explode(" ", $kumpulanHari);

    return $arrayHari[$angka-1];
  }



}
?>