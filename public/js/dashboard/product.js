function product(chartName,result){
    crt =   {
                options : {
                    chart: {
                        type: 'column',
                        backgroundColor:'transparent'
                    },
                    title: {
                        text: 'Product'
                    },
                    subtitle: {
                        text: 'Source: SummaryDashboard'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Dalam Jutaan Rupiah'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f} Juta</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        color: '#FF5733',
                        name: 'SIN',
                        data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1]

                    }, {
                        color: '#FFD133',
                        name: 'REG',
                        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5]

                    }, {
                        color: '#C70039',
                        name: 'Total',
                        data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2]

                    }]

                },

                generateChart: function(chartName,result) {
                    crt.options.xAxis.categories = result.kategori;
                    crt.options.series[0].data = result.sin;
                    crt.options.series[1].data = result.reg;
                    crt.options.series[2].data = result.total;
                    Highcharts.chart(chartName, crt.options);
                }

            };

    crt.generateChart(chartName,result);
}
