

<div class="kt-portlet">
    <div class="kt-portlet__head">
        <div class="kt-portlet__head-label">
            <h3 class="kt-portlet__head-title">
                 Manajemen Role (Ubah)
            </h3>
        </div>
    </div>
    <!--begin::Form-->
    <form name="fmenurole" id="fmenurole" class="kt-form kt-form--label-right" action="{{'menu_role/edit_data'}}" method="post" onsubmit="return false;"> 

        @csrf
        <input type="hidden" name="act" id="act" value="update" onchange="act();" readonly="readonly">
        <input name="role_id" id="role_id" type="hidden" value="{{$role_id}}">
		<input name="idmenu" id="idmenu" type="hidden" value="{{$idmenu}}">
		
		 <div class="kt-portlet__body">
            <div class="row">
				<div class="col-md-12">
					<div class="row kt-margin-b-20">
						<div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
							<label>Nama Role:</label>
							<input id="NmRole" name="NmRole" value="{{ $NmRole  }}" type="text" class="form-control kt-input">
						</div>						
						<div class="col-lg-4 kt-margin-b-10-tablet-and-mobile">
							<label>Status:</label>
							  {!! $status !!}
						</div>						
					</div>
				</div>
			</div>
		</div>
		
		
		
        <div class="kt-portlet__body" style="margin-top:-35px;";>
            <div class="row">
				<div class="col-md-12">
					<div class="form-group row">   
						<!--<label class="col-md-3 col-form-label">Menu</label>
						<div class="col-md-9">
							<div class="kt-checkbox-list">
								{!! $html !!}
							</div>
						</div>
					</div>-->
					{!! $html !!}
					<br>

				</div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button onclick="save_post_msg_update('fmenurole');"  class="btn btn-success">Simpan</button>
                       <!--  <button onclick="save_post_msg('user/add_data','_content_','Master Client', 'fuser');"  class="btn btn-success">Submit</button> -->
                        <button type="reset" onclick="call('menu_role','_content_','Role Management',{{$idmenu}})" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
	$(document).ready(function() {
        $('.select2').select2();
	});
	  
	  
    function rmvDsl($id) {
        var chkd = $('#IdMsMenu' + $id).is(':checked');
        if (chkd) {
            $('#IdMsRole' + $id).prop('disabled', false);
        } else {
            $('#IdMsRole' + $id).prop('disabled', true);
        }
    }
</script>