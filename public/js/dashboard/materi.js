function materi(chartName,result){
    crt =   {
                options : {
                    chart: {
                        type: 'column',
                        backgroundColor:'transparent'
                    },
                    title: {
                        text: 'Materi'
                    },
                    subtitle: {
                        text: 'Source: SummaryDashboard'
                    },
                    xAxis: {
                        categories: [
                            'Jan',
                            'Feb',
                            'Mar',
                            'Apr',
                            'May',
                            'Jun',
                            'Jul',
                            'Aug',
                            'Sep',
                            'Oct'
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: ' '
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: [{
                        color: '#FFD133',
                        name: 'Agen Baru',
                        data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5]
                    }]

                },

                generateChart: function(chartName,result) {
                    crt.options.xAxis.categories = result.kategori;
                    crt.options.series[0].data = result.AgenBaru;
                    Highcharts.chart(chartName, crt.options);
                }

            };

    crt.generateChart(chartName,result);
}
